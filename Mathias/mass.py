import numpy as np
import mne ## Handles EEG data
import matplotlib.pyplot as plt
from helper import read_patient_edfs

files = ["data/chbmit/chb{:02d}".format(x) for x in np.arange(1, 25)]

kwargs = dict(fmin=0.5, fmax=49.75, n_jobs=1)
picks = ["FP1-F7"]
tmin = 0.0
baseline = None

patientsArray = []
edfsArray = []
eventsArray = []
labelsArray = []
meansArray = []
mediansArray = []


for i_patient, file in enumerate(files):
    edfs = read_patient_edfs(file)

    for i_edf, data in enumerate(edfs):
        if len(data.annotations) == 0:
            continue

        epochsArray = []
        onsets = data.annotations.onset
        durations = data.annotations.duration
        events, event_id = mne.events_from_annotations(data)
        N_seizures = len(data.annotations)

        for i_event, event in enumerate(events):
            tmax = durations[i_event]

            epochSeizure = mne.Epochs(data, events, event_id, tmin, tmax, picks=picks, baseline=baseline, preload=True)
            patientsArray.append(i_patient)
            edfsArray.append(i_edf)
            eventsArray.append(i_event + 1)
            labelsArray.append(1)
            epochsArray.append(epochSeizure)

            epochNormal = mne.Epochs(data, np.array([[0, 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax, baseline=baseline, preload=True)
            patientsArray.append(i_patient)
            edfsArray.append(i_edf)
            eventsArray.append(0)
            labelsArray.append(0)
            epochsArray.append(epochNormal)

        for epoch in epochsArray:
            psds_welch_mean, freqs_mean = mne.time_frequency.psd_welch(epoch, average='mean', **kwargs)
            psds_welch_median, freqs_median = mne.time_frequency.psd_welch(epoch, average='median', **kwargs)

            # Convert power to dB scale.
            psds_welch_mean = 10 * np.log10(psds_welch_mean)
            psds_welch_median = 10 * np.log10(psds_welch_median)

            meansArray.append(psds_welch_mean)
            mediansArray.append(psds_welch_median)


f = open("patients.txt", "w")
f.write(str(patientsArray))
f.close()

f = open("edfs.txt", "w")
f.write(str(edfsArray))
f.close()


f = open("events.txt", "w")
f.write(str(eventsArray))
f.close()

f = open("labels.txt", "w")
f.write(str(labelsArray))
f.close()

f = open("means.txt", "w")
f.write(str(meansArray))
f.close()

f = open("medians.txt", "w")
f.write(str(mediansArray))
f.close()




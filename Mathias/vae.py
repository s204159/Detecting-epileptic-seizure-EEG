import keras
from keras import layers
import numpy as np
from keras.datasets import mnist
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import random
import scipy.signal as signal

## set random seed, still some randomness due to GPU
rs = 42
random.seed(rs)
np.random.seed(rs)


sampling_rate = 256
full_time_series = False
n_epochs = 100
n_patients = 1

## From https://blog.keras.io/building-autoencoders-in-keras.html

### From Christian.chrononet_keras2.ipynb
from glob import glob
import re
from tqdm.auto import tqdm
import mne
def patient_edfs_to_numpy(path_to_folder,epoch_length,sample_rate_hz = 256, downsample_rate = 1):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    epochs = []
    labels = []
    record_session = []
    patient_id = []

    summary_path = glob(path_to_folder + '/*summary.txt')

    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):

            # continues to next edf file
            if lines[i][:10] != 'File Name:':
                i += 1
                continue

            # Gets our new edf file and number of seizures from summary
            edf_file_path = path_to_folder +'/'+ lines[i][11:]
            number_seizures = int(lines[i+3][-1])

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path,preload=True,verbose='ERROR')
            edf.set_eeg_reference()

            #skips if FP1-F7 does not exist
            if 'FP1-F7' not in edf.info['ch_names']:
                i += 1
                continue

            #edf = edf.pick_channels(['FP1-F7','F7-T7'])
            edf = edf.pick_channels(['FP1-F7', ])#'F7-T7', 'T7-P7', 'P7-O1', 'FP1-F3', 'F3-C3'])
            edf_data = edf.get_data()

            splitedSize = epoch_length*sample_rate_hz
            current_location = 0
            for k in range(number_seizures):
                onset = int(re.findall('\d+', lines[i+4+k*2])[-1])
                duration = int(re.findall('\d+', lines[i+5+k*2])[-1]) - onset

                edf_splitted = [edf_data[:,x:x+splitedSize] for x in range(current_location*sample_rate_hz, onset*sample_rate_hz, splitedSize) if x+splitedSize<=edf_data.shape[1]]
                for split in edf_splitted[::downsample_rate]:
                    epochs.append(split)
                    labels.append(0)
                    record_session.append(lines[i][11:])
                    patient_id.append(path_to_folder[-2:])
                current_location = onset

                edf_splitted = [edf_data[:,x:x+splitedSize] for x in range(current_location*sample_rate_hz, (current_location+duration)*sample_rate_hz, splitedSize)  if x+splitedSize<=edf_data.shape[1]]
                for split in edf_splitted:
                    epochs.append(split)
                    labels.append(1)
                    record_session.append(lines[i][11:])
                    patient_id.append(path_to_folder[-2:])
                current_location = current_location+duration

            edf_splitted = [edf_data[:,x:x+splitedSize] for x in range(current_location*sample_rate_hz,len(edf_data), splitedSize)  if x+splitedSize<=edf_data.shape[1]]
            for split in edf_splitted[::downsample_rate]:
                    epochs.append(split)
                    labels.append(0)
                    record_session.append(lines[i][11:])
                    patient_id.append(path_to_folder[-2:])
            i += 1
    return np.array(epochs), np.array(labels), np.array(record_session),np.array(patient_id)

def read_patients_edfs(patient_numbers):
    '''
    Reads all patients edf files
    :return:
    '''
    epochs = np.array([])
    labels = np.array([])
    record_session = np.array([])
    patients = np.array([])
    for patient_number in tqdm(patient_numbers):
        patient_epochs, patient_labels, patient_record_session,patients_ids = patient_edfs_to_numpy(f'../data/chbmit/chb{patient_number:02d}',4)
        if len(epochs) != 0:
            epochs = np.vstack((epochs,patient_epochs))
            labels = np.hstack((labels,patient_labels))
            patients = np.hstack((patients,patients_ids))
            record_session = np.hstack((record_session, patient_record_session))
        else:
            epochs = patient_epochs
            labels = patient_labels
            patients = patients_ids
            record_session = patient_record_session

    return epochs, labels, record_session, patients

#epochs, labels, record_session, patient_ids = read_patients_edfs([i for i in range(1,12)])
epochs, labels, record_session, patient_ids = read_patients_edfs([i for i in range(1, n_patients+1)])

epochs = np.moveaxis(epochs,1,2)
print(epochs.shape, labels.shape, record_session.shape)
### End from Christian.chrononet_keras2.ipynb

if not full_time_series:
    epochsArray = epochs
    mediansArray = []
    # le = 0
    for epoch in epochsArray:
        freqs_median, psds_welch_median = signal.welch(epoch[:, 0], fs=sampling_rate, nperseg=256, average='median')

        # Convert power to dB scale.
        # psds_welch_median = 10 * np.log10(psds_welch_median)

        # plot a couple of spectrograms
        # if le < 10:
        #     plt.semilogy(freqs_median, psds_welch_median)
        #     plt.xlabel('frequency [Hz]')
        #     plt.ylabel('PSD [V**2/Hz]')
        #     plt.show()
        # le+=1

        mediansArray.append(psds_welch_median[0:51])
    epochs = np.array(mediansArray)
    epochs = np.array([[(d-np.min(l))/(np.max(l)-np.min(l)) for d in l] for l in epochs])
print(epochs.shape)

### Split into seizures and nonseizures, as the VAE trains on nonseizure data
no_seizure = epochs[labels == 0]
seizure = epochs[labels == 1]
print(no_seizure.shape)
print(seizure.shape)
print(np.concatenate((no_seizure[1::2], seizure)).shape)
###

original_dim = 51
intermediate_dim = 8
latent_dim = 4
batch_size = 32

if full_time_series:
    original_dim = 1024
    intermediate_dim = 64
    latent_dim = 12
    batch_size = 32

inputs = keras.Input(shape=(original_dim,))
h = layers.Dense(intermediate_dim, activation='relu')(inputs)
z_mean = layers.Dense(latent_dim)(h)
z_log_sigma = layers.Dense(latent_dim)(h)

from keras import backend as K

def sampling(args):
    z_mean, z_log_sigma = args
    epsilon = K.random_normal(shape=(K.shape(z_mean)[0], latent_dim),
                              mean=0., stddev=0.1)
    return z_mean + K.exp(z_log_sigma) * epsilon

z = layers.Lambda(sampling)([z_mean, z_log_sigma])

# Create encoder
encoder = keras.Model(inputs, [z_mean, z_log_sigma, z], name='encoder')

# Create decoder
latent_inputs = keras.Input(shape=(latent_dim,), name='z_sampling')
x = layers.Dense(intermediate_dim, activation='relu')(latent_inputs)
outputs = layers.Dense(original_dim, activation='sigmoid')(x)
decoder = keras.Model(latent_inputs, outputs, name='decoder')

# instantiate VAE model
outputs = decoder(encoder(inputs)[2])
vae = keras.Model(inputs, outputs, name='vae_mlp')

reconstruction_loss = keras.losses.binary_crossentropy(inputs, outputs)
reconstruction_loss *= original_dim
kl_loss = 1 + z_log_sigma - K.square(z_mean) - K.exp(z_log_sigma)
kl_loss = K.sum(kl_loss, axis=-1)
kl_loss *= -0.5
vae_loss = K.mean(reconstruction_loss + kl_loss)
vae.add_loss(vae_loss)
vae.compile(optimizer='adam')

cutoff = no_seizure.shape[0] - seizure.shape[0]
(x_train, y_train), (x_test, y_test) = (no_seizure[0:cutoff], labels[labels == 0][0:cutoff]), (np.concatenate((no_seizure[cutoff:], seizure)), np.concatenate((labels[labels==0][cutoff:], labels[labels == 1])))
# cutoff = int(no_seizure.shape[0]*0.9)
# (x_train, y_train), (x_test, y_test) = (no_seizure[0:cutoff], labels[labels == 0][0:cutoff]), (np.concatenate((no_seizure[cutoff:], seizure)), np.concatenate((labels[labels==0][cutoff:], labels[labels == 1])))
# (x_train, y_train), (x_test, y_test) = (no_seizure[0::2], labels[labels == 0][0::2]), (np.concatenate((no_seizure[1::2], seizure)), np.concatenate((labels[labels==0][1::2], labels[labels == 1])))
print(x_train.shape, y_train.shape, x_test.shape, y_test.shape)

x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

vae.fit(x_train, x_train,
        epochs=n_epochs,
        batch_size=batch_size,
        validation_data=(x_test, x_test))

#x_test_encoded = encoder.predict(x_test, batch_size=32)
#x_test_decoded = decoder.predict(x_test_encoded, batch_size=32)

## From https://www.tensorflow.org/tutorials/generative/autoencoder
reconstructions = vae.predict(x_test, batch_size=32)
train_reconstructions = vae.predict(x_train, batch_size=32)
recloss = keras.losses.binary_crossentropy(reconstructions, x_test)
trainloss = keras.losses.binary_crossentropy(train_reconstructions, x_train)
print(reconstructions.shape)
print(recloss.shape)
print(x_test.shape)
print(np.squeeze(np.array([recloss[y_test == 0][None, :], recloss[y_test == 1][None, :]])).shape)
plt.hist(np.squeeze(np.array([recloss[y_test == 0][None, :], recloss[y_test == 1][None, :]])).T, bins=50, stacked=True, color=["blue", "red"], label=['Non-seizure', 'Seizure'])

plt.xlabel("Test loss")
plt.ylabel("No of examples")
#plt.title("{intermediate_dim} {latent_dim} {batch_size}".format(intermediate_dim, latent_dim, batch_size))
plt.show()

std_ratio = 1
threshold = np.mean(trainloss + (np.std(recloss)*std_ratio))
# thresholdratio = 1
# threshold *= thresholdratio
print("Threshold: ", threshold, " with std dev ratio:", std_ratio)
pred_labels = recloss >= threshold
correct = np.sum(pred_labels == y_test)
acc = correct/y_test.shape[0]
print(acc)

pred_labels = pred_labels.numpy()

tp = pred_labels[pred_labels == y_test]
tp = np.sum(tp == 1)
fp = pred_labels[pred_labels != y_test]
fp = np.sum(fp == 1)
tn = pred_labels[pred_labels == y_test]
tn = np.sum(tn == 0)
fn = pred_labels[pred_labels != y_test]
fn = np.sum(fn == 0)
tpr = tp/(tp+fn)
tnr = tn/(tn+fp)
fpr = fp/(fp+tn)
fnr = fn/(fn+tp)

print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)

## try sorting to see if the reconstruction error is highest during seizures
maxes_i = np.argpartition(recloss, -1*seizure.shape[0])[-1*seizure.shape[0]:]
pred_labels = np.zeros_like(y_test)
pred_labels[maxes_i] = 1
correct = np.sum(pred_labels == y_test)
acc = correct/y_test.shape[0]
print(acc)
tp = pred_labels[pred_labels == y_test]
tp = np.sum(tp == 1)
fp = pred_labels[pred_labels != y_test]
fp = np.sum(fp == 1)
tn = pred_labels[pred_labels == y_test]
tn = np.sum(tn == 0)
fn = pred_labels[pred_labels != y_test]
fn = np.sum(fn == 0)
tpr = tp/(tp+fn)
tnr = tn/(tn+fp)
fpr = fp/(fp+tn)
fnr = fn/(fn+tp)

print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)

decoded_data = vae.predict(x_test, batch_size=32)[48]
plt.plot(x_test[48], 'b')
plt.plot(decoded_data, 'r')
plt.fill_between(np.arange(original_dim), decoded_data, x_test[48], color='lightcoral')
plt.legend(labels=["Input", "Reconstruction", "Error"])
plt.show()

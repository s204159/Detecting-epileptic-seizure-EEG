import numpy as np
import mne ## Handles EEG data
import matplotlib.pyplot as plt
from glob import glob
import re


def read_patient_edfs(path):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    summary_path = glob(path + '/*summary.txt')
    edfs = []

    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):
            # continues to next file
            if lines[i][:10] != 'File Name:':
                i += 1
                continue

            # Gets our new edf file and number of seizures from summary
            edf_file_path = path +'/'+ lines[i][11:]
            number_seizures = int(lines[i+3][-1])

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path,preload=True)

            onsets = []
            ends = []
            for k in range(number_seizures):
                onsets.append(re.findall('\d+', lines[i+4+k*2])[0])
                ends.append(re.findall('\d+', lines[i+5+k*2])[0])
            durations = np.array(ends).astype(float) - np.array(onsets).astype(float)

            if number_seizures != 0:
                edf_annotations = mne.Annotations(onset=onsets,
                              duration=durations,
                              description=number_seizures*['Seizure'])
                edf = edf.set_annotations(edf_annotations)

            edfs.append(edf)
            i += 1

    return edfs


file = "../data/chbmit/chb03"
edfs = read_patient_edfs(file)

data = edfs[0]
#data = mne.io.read_raw_edf(edf3)
raw_data = data.get_data()
sfreq = data.info['sfreq']

picks = ["FP1-F7"]

onsets = data.annotations.onset
durations = data.annotations.duration
events, event_id = mne.events_from_annotations(data)

print(events)
## Construct Epochs
tmin, tmax = 0.0, durations[0]
print("time:" + str(durations[0]))
baseline = None #(None, 0)
epochsSeizure = mne.Epochs(data, events, event_id, tmin, tmax, picks=picks, baseline=baseline, preload=True)
epochsNormal1 = mne.Epochs(data, np.array([[0, 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax, baseline=baseline, preload=True)
epochsNormal2 = mne.Epochs(data, np.array([[int(onsets[0]+durations[0]), 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax, baseline=baseline, preload=True)
#epochsNormal1 = mne.Epochs(data, np.array([[0, 0, 2]]), {'Normal': 2}, tmin=0.0, tmax=onsets[0], baseline=baseline, preload=True)
#epochsNormal2 = mne.Epochs(data, np.array([[int(onsets[0]+durations[0]), 0, 2]]), {'Normal': 2}, tmin=0.0, tmax=(float(len(data))/sfreq)-(onsets[0]+durations[0]), baseline=baseline, preload=True)


epochsArray = [epochsNormal1, epochsSeizure, epochsNormal2]
# Estimate PSDs based on "mean" and "median" averaging for comparison.
for epochs in epochsArray:
    kwargs = dict(fmin=0.5, fmax=49.75, n_jobs=1)
    psds_welch_mean, freqs_mean = mne.time_frequency.psd_welch(epochs, average='mean', **kwargs)
    psds_welch_median, freqs_median = mne.time_frequency.psd_welch(epochs, average='median', **kwargs)

    # Convert power to dB scale.
    psds_welch_mean = 10 * np.log10(psds_welch_mean)
    psds_welch_median = 10 * np.log10(psds_welch_median)

    # We will only plot the PSD for a single sensor in the first epoch.
    ch_name = "FP1-F7"
    ch_idx = epochs.info['ch_names'].index(ch_name)
    epo_idx = 0

    _, ax = plt.subplots()
    ax.plot(freqs_mean, psds_welch_mean[epo_idx, ch_idx, :], color='k',
            ls='-', label='mean of segments')
    ax.plot(freqs_median, psds_welch_median[epo_idx, ch_idx, :], color='k',
            ls='--', label='median of segments')

    ax.set(title='Welch PSD ({}, Epoch {})'.format(ch_name, epo_idx),
           xlabel='Frequency (Hz)', ylabel='Power Spectral Density (dB)')
    ax.legend(loc='upper right')
    plt.show()

#
# mne.time_frequency.psd_welch(data, fmin=0.5, fmax=49.75)

#filter_params = mne.filter.create_filter(data=data, sfreq=sfreq, l_freq=0.5, h_freq=49.75, method='fir')


# ica = mne.preprocessing.ICA(n_components=20, random_state=21, max_iter=800)
# ica.fit(data)
# ica.exclude = [1, 2]
# ica.plot_properties(data, picks=ica.exclude)


## Construct Epochs
tmin, tmax = 0.0, 4.0
baseline = None #(None, 0)
epochsSeizure = mne.Epochs(data, events, event_id, tmin, tmax, picks=picks, baseline=baseline, preload=True)
epochsNormal1 = mne.Epochs(data, np.array([[0, 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax, baseline=baseline, preload=True)
epochsNormal2 = mne.Epochs(data, np.array([[int(onsets[0]+durations[0]), 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax, baseline=baseline, preload=True)
#epochsNormal1 = mne.Epochs(data, np.array([[0, 0, 2]]), {'Normal': 2}, tmin=0.0, tmax=onsets[0], baseline=baseline, preload=True)
#epochsNormal2 = mne.Epochs(data, np.array([[int(onsets[0]+durations[0]), 0, 2]]), {'Normal': 2}, tmin=0.0, tmax=(float(len(data))/sfreq)-(onsets[0]+durations[0]), baseline=baseline, preload=True)


epochsArray = [epochsNormal1, epochsSeizure, epochsNormal2]
# Estimate PSDs based on "mean" and "median" averaging for comparison.
for epochs in epochsArray:
    kwargs = dict(fmin=0.5, fmax=49.75, n_jobs=1)
    psds_welch_mean, freqs_mean = mne.time_frequency.psd_welch(epochs, average='mean', **kwargs)
    psds_welch_median, freqs_median = mne.time_frequency.psd_welch(epochs, average='median', **kwargs)

    # Convert power to dB scale.
    psds_welch_mean = 10 * np.log10(psds_welch_mean)
    psds_welch_median = 10 * np.log10(psds_welch_median)

    # We will only plot the PSD for a single sensor in the first epoch.
    ch_name = "FP1-F7"
    ch_idx = epochs.info['ch_names'].index(ch_name)
    epo_idx = 0

    _, ax = plt.subplots()
    ax.plot(freqs_mean, psds_welch_mean[epo_idx, ch_idx, :], color='k',
            ls='-', label='mean of segments')
    ax.plot(freqs_median, psds_welch_median[epo_idx, ch_idx, :], color='k',
            ls='--', label='median of segments')

    ax.set(title='Welch PSD ({}, Epoch {})'.format(ch_name, epo_idx),
           xlabel='Frequency (Hz)', ylabel='Power Spectral Density (dB)')
    ax.legend(loc='upper right')
    plt.show()

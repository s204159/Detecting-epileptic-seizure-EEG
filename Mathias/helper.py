import numpy as np
import mne ## Handles EEG data
import matplotlib.pyplot as plt
from glob import glob
import re


def read_patient_edfs(path):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    summary_path = glob(path + '/*summary.txt')
    edfs = []

    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):
            # continues to next file
            if lines[i][:10] != 'File Name:':
                i += 1
                continue

            # Gets our new edf file and number of seizures from summary
            edf_file_path = path +'/'+ lines[i][11:]
            number_seizures = int(lines[i+3][-1])

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path,preload=True)

            onsets = []
            ends = []
            for k in range(number_seizures):
                onsets.append(re.findall('\d+', lines[i+4+k*2])[-1])
                ends.append(re.findall('\d+', lines[i+5+k*2])[-1])
            durations = np.array(ends).astype(float) - np.array(onsets).astype(float)

            if number_seizures != 0:
                edf_annotations = mne.Annotations(onset=onsets,
                              duration=durations,
                              description=number_seizures*['Seizure'])
                edf = edf.set_annotations(edf_annotations)

            edfs.append(edf)
            i += 1

    return edfs


def convert_patient_edfs(path1, path2):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    summary_path = glob(path1 + '/*summary.txt')
    output_path = glob(path2)
    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):
            # continues to next file
            if lines[i][:10] != 'File Name:':
                i += 1
                continue

            # Gets our new edf file and number of seizures from summary
            edf_file_path = path1 +'/'+ lines[i][11:]
            number_seizures = int(lines[i+3][-1])

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path,preload=True)

            onsets = []
            ends = []
            for k in range(number_seizures):
                onsets.append(re.findall('\d+', lines[i+4+k*2])[-1])
                ends.append(re.findall('\d+', lines[i+5+k*2])[-1])
            durations = np.array(ends).astype(float) - np.array(onsets).astype(float)

            if number_seizures != 0:
                edf_annotations = mne.Annotations(onset=onsets,
                              duration=durations,
                              description=number_seizures*['Seizure'])
                edf = edf.set_annotations(edf_annotations)
            edf = edf.set_meas_date((0, 0))
            target_file_path = (path2 + '/' + lines[i][11:])[:-3] + 'fif'

            edf.save(target_file_path, ['FP1-F7'], overwrite=True)
            i += 1


def read_fifs(path):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    read_path = glob(path + '/*.fif')
    fifs = []

    for file in read_path:
        fifs.append(mne.io.read_raw_fif(file, preload=True))

    return fifs
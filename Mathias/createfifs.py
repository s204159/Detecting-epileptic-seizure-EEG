from Mathias.helper import convert_patient_edfs
import numpy as np

file = "../data/chbmit/chb01"
out = "data/out/chb01"

files = ["data/chbmit/chb{:02d}".format(x) for x in np.arange(4, 25)]
outs = ["data/out/chb{:02d}".format(x) for x in np.arange(4, 25)]

for file, out in zip(files, outs):
    convert_patient_edfs(file, out)

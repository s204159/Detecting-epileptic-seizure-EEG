import numpy as np
import mne ## Handles EEG data
import matplotlib.pyplot as plt
import scipy.fft as fft



file = "../data/chb01/chb01_01.edf"
data = mne.io.read_raw_edf(file)
raw_data = data.get_data()
info = data.info
channels = data.ch_names

print(len(data.annotations))
print(info)
print(channels)

fp1f7 = data['FP1-F7']
#fp1f7 = np.array(fp1f7, dtype=object)

print("Number of channels: ", str(len(raw_data)))
print("Number of samples: ", str(len(raw_data[0])))

print(fp1f7)

normalized_tone = np.int16((raw_data[0,:921600]/raw_data[0,:921600].max())*32767)

#plt.plot(normalized_tone[0:1000])
#plt.show()

N = 921600
s = 921600/256

yf = fft.fft(normalized_tone)
xf = fft.fftfreq(N, 1 / 256.0)

#plt.plot(raw_data[0,:921600])
plt.plot(xf, np.abs(yf))
plt.title("Raw EEG, electrode 0 (FP1-F7), samples 0-921600")
plt.show()

data.plot(duration=30, n_channels=23)
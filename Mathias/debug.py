import numpy as np
from glob import glob
import re
import mne


def read_patient_edfs(path):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    summary_path = glob(path + '/*summary.txt')
    edfs = []

    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):
            # continues to next file
            if lines[i][:10] != 'File Name:':
                i += 1
                continue

            # Gets our new edf file and number of seizures from summary
            edf_file_path = path +'/'+ lines[i][11:]
            number_seizures = int(lines[i+3][-1])

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path,preload=True)

            onsets = []
            ends = []
            for k in range(number_seizures):
                onsets.append(re.findall('\d+', lines[i+4+k*2])[0])
                ends.append(re.findall('\d+', lines[i+5+k*2])[0])
            durations = np.array(ends).astype(float) - np.array(onsets).astype(float)

            if number_seizures != 0:
                edf_annotations = mne.Annotations(onset=onsets,
                              duration=durations,
                              description=number_seizures*['Seizure'])
                edf = edf.set_annotations(edf_annotations)

            edfs.append(edf)
            i += 1

    return edfs


read_patient_edfs('../data/chbmit/chb04')
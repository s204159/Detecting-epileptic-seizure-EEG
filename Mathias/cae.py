## Imports
import sklearn.model_selection
import tensorflow as tf
from tensorflow import keras as keras
# import keras
# from keras import layers
# from keras import backend as K
import numpy as np
from keras.datasets import mnist
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import random
import scipy.signal as signal

## Tensorflow stuff
physical_devices = tf.config.list_physical_devices('GPU')
print(physical_devices)
tf.config.set_visible_devices(devices=physical_devices[0], device_type='GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], enable=True)
# tf.config.run_functions_eagerly(True)

## Set random seed, still some randomness due to using GPU
rs = 42
random.seed(rs)
np.random.seed(rs)

## Set parameters
sampling_rate = 256
full_time_series = False
n_epochs = 10
patient_start = 1
n_patients = 1
lr = 5e-4

## From https://blog.keras.io/building-autoencoders-in-keras.html (hvilken del?)

### From Christian.chrononet_keras2.ipynb
from glob import glob
import re
from tqdm.auto import tqdm
import mne
def patient_edfs_to_numpy(path_to_folder,epoch_length,sample_rate_hz = 256, downsample_rate = 1):
    '''
    Loads all edf files and annotations from a patient folder
    '''
    epochs = []
    labels = []
    record_session = []
    patient_id = []

    summary_path = glob(path_to_folder + '/*summary.txt')

    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):

            # continues to next edf file
            if lines[i][:10] != 'File Name:':
                i += 1
                continue

            # Gets our new edf file and number of seizures from summary
            edf_file_path = path_to_folder +'/'+ lines[i][11:]
            number_seizures = int(lines[i+3][-1])

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path,preload=True,verbose='ERROR')
            edf.set_eeg_reference()

            #skips if FP1-F7 does not exist
            if 'FP1-F7' not in edf.info['ch_names']:
                i += 1
                continue

            #edf = edf.pick_channels(['FP1-F7','F7-T7'])
            #edf = edf.pick_channels(['FP1-F7', ])#'F7-T7', 'T7-P7', 'P7-O1', 'FP1-F3', 'F3-C3'])
            # re-reference to FP1-FP2
            edf = edf.pick_channels(['FP1-F7', 'F7-T7', 'T7-FT9', 'FT9-FT10', 'FT10-T8', 'F8-T8', 'FP2-F8'])
            fp1f7 = edf.get_data(['FP1-F7'])
            f7t7 = edf.get_data(['F7-T7'])
            t7ft9 = edf.get_data(['T7-FT9'])
            ft9ft10 = edf.get_data(['FT9-FT10'])
            ft10t8 = edf.get_data(['FT10-T8'])
            f8t8 = edf.get_data(['F8-T8'])
            fp2f8 = edf.get_data(['FP2-F8'])
            ## Formula for reconstructing FP1-FP2 signal in CHB-MIT dataset from Yanjun at Curvex
            edf_data = fp1f7+f7t7+t7ft9+ft9ft10+ft10t8-f8t8-fp2f8
            #edf_data = edf.get_data()

            # apply cutoff filter to data (4th order butterworth between 0.5 and 49.75 hz)
            edf_data = mne.filter.filter_data(edf_data, sfreq=sample_rate_hz, l_freq=0.5, h_freq=49.75, method="iir", verbose=False)


            splitedSize = epoch_length*sample_rate_hz
            current_location = 0
            for k in range(number_seizures):
                onset = int(re.findall('\d+', lines[i+4+k*2])[-1])
                duration = int(re.findall('\d+', lines[i+5+k*2])[-1]) - onset

                edf_splitted = [edf_data[:,x:x+splitedSize] for x in range(current_location*sample_rate_hz, onset*sample_rate_hz, splitedSize) if x+splitedSize<=edf_data.shape[1]]
                for split in edf_splitted[::downsample_rate]:
                    epochs.append(split)
                    labels.append(0)
                    record_session.append(lines[i][11:])
                    patient_id.append(path_to_folder[-2:])
                current_location = onset

                edf_splitted = [edf_data[:,x:x+splitedSize] for x in range(current_location*sample_rate_hz, (current_location+duration)*sample_rate_hz, splitedSize)  if x+splitedSize<=edf_data.shape[1]]
                for split in edf_splitted:
                    epochs.append(split)
                    labels.append(1)
                    record_session.append(lines[i][11:])
                    patient_id.append(path_to_folder[-2:])
                current_location = current_location+duration

            edf_splitted = [edf_data[:,x:x+splitedSize] for x in range(current_location*sample_rate_hz,len(edf_data), splitedSize)  if x+splitedSize<=edf_data.shape[1]]
            for split in edf_splitted[::downsample_rate]:
                    epochs.append(split)
                    labels.append(0)
                    record_session.append(lines[i][11:])
                    patient_id.append(path_to_folder[-2:])
            i += 1
    return np.array(epochs), np.array(labels), np.array(record_session),np.array(patient_id)

def read_patients_edfs(patient_numbers):
    '''
    Reads all patients edf files
    :return:
    '''
    epochs = np.array([])
    labels = np.array([])
    record_session = np.array([])
    patients = np.array([])
    for patient_number in tqdm(patient_numbers):
        patient_epochs, patient_labels, patient_record_session,patients_ids = patient_edfs_to_numpy(f'../data/chbmit/chb{patient_number:02d}',4)
        if len(epochs) != 0:
            epochs = np.vstack((epochs,patient_epochs))
            labels = np.hstack((labels,patient_labels))
            patients = np.hstack((patients,patients_ids))
            record_session = np.hstack((record_session, patient_record_session))
        else:
            epochs = patient_epochs
            labels = patient_labels
            patients = patients_ids
            record_session = patient_record_session

    return epochs, labels, record_session, patients

#epochs, labels, record_session, patient_ids = read_patients_edfs([i for i in range(1,12)])
epochs, labels, record_session, patient_ids = read_patients_edfs([i for i in range(patient_start, patient_start+n_patients)])

epochs = np.moveaxis(epochs,1,2)
print(epochs.shape, labels.shape, record_session.shape)
### End from Christian.chrononet_keras2.ipynb

if not full_time_series:
    epochsArray = epochs
    mediansArray = []
    # le = 0
    for epoch in epochsArray:
        freqs_median, psds_welch_median = signal.welch(epoch[:, 0], fs=sampling_rate, nperseg=256, average='median')

        # Convert power to dB scale.
        # psds_welch_median = 10 * np.log10(psds_welch_median)

        # plot a couple of spectrograms
        # if le < 10:
        #     plt.semilogy(freqs_median, psds_welch_median)
        #     plt.xlabel('frequency [Hz]')
        #     plt.ylabel('PSD [V**2/Hz]')
        #     plt.show()
        # le+=1
        psds_welch_median = np.append(psds_welch_median, 0)  # zero-padding
        mediansArray.append(psds_welch_median[0:52])
    epochs = np.array(mediansArray)  # .astype("float32")
    # z-score normalization
    epochs = np.array([[(d - np.mean(l)) / (np.std(l) + (1e-30)) for d in l] for l in epochs])
    # epochs = np.array([[(d - np.mean(l)) / (np.std(l) + (1e-30)) for d in l] for l in epochs])
    # normalize between 0 and 1 (swaps amplitude for percentual amplitude (why?))
    epochs = np.array([[(d - np.min(l)) / (np.max(l) - np.min(l) + (1e-30)) for d in l] for l in epochs])
    #epochs = np.array([[(d - np.min(l)) / (np.max(l) - np.min(l) + (1e-30)) for d in l] for l in epochs])
    # convert to float32 for use in tensorflow
    epochs = epochs.astype("float32")

print(epochs.shape)

### Split into seizures and nonseizures, as the VAE trains on nonseizure data
no_seizure = epochs[labels == 0]
seizure = epochs[labels == 1]
print(no_seizure.shape)
print(seizure.shape)
print(np.concatenate((no_seizure[1::2], seizure)).shape)
###

original_dim = 52
intermediate_dim = 8
latent_dim = 13
batch_size = 32

if full_time_series:
    original_dim = 1024
    intermediate_dim = 64
    latent_dim = 12
    batch_size = 32

## Reparameterization trick, from https://keras.io/examples/generative/vae/
class Sampling(keras.layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim)) ## Normal distribution mean = 0.0, sigma = 1
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon

## Partly from https://www.tensorflow.org/tutorials/generative/cvae

## Overall structure from https://keras.io/examples/generative/vae/
## Rewritten as model classes, changed to one-dimensional, fixing various issues

class Encoder(keras.Model):
    def __init__(self, latent_dim):
        super(Encoder, self).__init__()
        self.latent_dim = latent_dim
        #self.inputs_ = keras.layers.InputLayer(input_shape=(original_dim, 1))
        self.inputs_ = keras.Input(shape=(original_dim, 1))
        self._set_input_layer(self.inputs_)
        self.conv1d_1 = keras.layers.Conv1D(filters=32, kernel_size=2, activation="relu", strides=1, padding="same")
        self.bn_1 = keras.layers.BatchNormalization()
        self.conv1d_2 = keras.layers.Conv1D(64, 3, activation="relu", strides=2, padding="same")
        self.bn_2 = keras.layers.BatchNormalization()
        self.conv1d_3 = keras.layers.Conv1D(64, 3, activation="relu", strides=2, padding="same")
        self.bn_3 = keras.layers.BatchNormalization()
        self.conv1d_4 = keras.layers.Conv1D(64, 2, activation="relu", strides=1, padding="same")
        self.bn_4 = keras.layers.BatchNormalization()
        self.flatten = keras.layers.Flatten()
        self.dense = keras.layers.Dense(16, activation="relu")
        self.z_mean = keras.layers.Dense(latent_dim, name="z_mean")
        self.z_log_var = keras.layers.Dense(latent_dim, name="z_log_var")
        self.z = Sampling()
        self.build()

    ## From AlwayProblem https://stackoverflow.com/questions/55235212/model-summary-cant-print-output-shape-while-using-subclass-model
    ## To get proper outputs from summary()
    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(Encoder, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def call(self, x):
        x = self.conv1d_1(x)
        x = self.bn_1(x)
        x = self.conv1d_2(x)
        x = self.bn_2(x)
        x = self.conv1d_3(x)
        x = self.bn_3(x)
        x = self.conv1d_4(x)
        x = self.bn_4(x)
        x = self.flatten(x)
        x = self.dense(x)
        z_mean = self.z_mean(x)
        z_log_var = self.z_log_var(x)
        z = self.z([z_mean, z_log_var])
        return [z_mean, z_log_var, z]

class Decoder(keras.Model):
    def __init__(self, latent_dim):
        super(Decoder, self).__init__()
        self.latent_dim = latent_dim
        self.inputs_ = keras.Input(shape=(latent_dim,))
        self._set_input_layer(self.inputs_)
        self.dense_1 = keras.layers.Dense(latent_dim, activation="relu")
        self.reshape_1 = keras.layers.Reshape((latent_dim, 1))
        self.conv1d_1 = keras.layers.Conv1DTranspose(64, 2, activation="relu", strides=1, padding="same")
        self.bn_1 = keras.layers.BatchNormalization()
        self.conv1d_2 = keras.layers.Conv1DTranspose(64, 3, activation="relu", strides=2, padding="same")
        self.bn_2 = keras.layers.BatchNormalization()
        self.conv1d_3 = keras.layers.Conv1DTranspose(64, 3, activation="relu", strides=2, padding="same")
        self.bn_3 = keras.layers.BatchNormalization()
        self.conv1d_4 = keras.layers.Conv1DTranspose(32, 2, activation="relu", strides=1, padding="same")
        self.bn_4 = keras.layers.BatchNormalization()
        self.flatten = keras.layers.Flatten()
        self.dense_2 = keras.layers.Dense(original_dim)
        self.reshape_2 = keras.layers.Reshape(target_shape=(original_dim, 1))
        self.decoder_outputs = keras.layers.Conv1DTranspose(1, 3, activation="sigmoid", padding="same")
        self.build()

    ## From AlwayProblem https://stackoverflow.com/questions/55235212/model-summary-cant-print-output-shape-while-using-subclass-model
    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(Decoder, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def call(self, x):
        x = self.dense_1(x)
        x = self.reshape_1(x)
        x = self.conv1d_1(x)
        x = self.bn_1(x)
        x = self.conv1d_2(x)
        x = self.bn_2(x)
        x = self.conv1d_3(x)
        x = self.bn_3(x)
        x = self.conv1d_4(x)
        x = self.bn_4(x)
        x = self.flatten(x)
        x = self.dense_2(x)
        x = self.reshape_2(x)
        x = self.decoder_outputs(x)
        return x


class ClassifierNN(keras.Model):
    def __init__(self, latent_dim):
        super(ClassifierNN, self).__init__()
        self.latent_dim = latent_dim
        self.inputs_ = keras.Input(shape=(latent_dim,))
        self._set_input_layer(self.inputs_)
        self.dense_1 = keras.layers.Dense(latent_dim, activation="relu")
        self.dense_2 = keras.layers.Dense(6, activation="relu")
        self.dense_3 = keras.layers.Dense(3, activation="relu")
        self.dense_4 = keras.layers.Dense(1, activation="sigmoid")
        self.build()

    ## From AlwayProblem https://stackoverflow.com/questions/55235212/model-summary-cant-print-output-shape-while-using-subclass-model
    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(ClassifierNN, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def call(self, x):
        x = self.dense_1(x)
        x = self.dense_2(x)
        x = self.dense_3(x)
        x = self.dense_4(x)
        return x

class CVAE(keras.Model):
    def __init__(self, encoder, decoder, classifier, **kwargs):
        super(CVAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.classifier = classifier
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.percent_loss_tracker = keras.metrics.Mean(name="percent_loss")
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")
        self.classifier_bce_tracker = keras.metrics.Mean(name="classifier_bce")


    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
            self.percent_loss_tracker,
            self.classifier_bce_tracker,
        ]

    def train_step(self, data):
        data, labels = data
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            prediction = self.classifier(z)
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.binary_crossentropy(data, reconstruction), axis=1
                )
            )
            #reconstruction_loss = keras.losses.MeanSquaredError(data, reconstruction)
            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            #print(data.shape)
            #print(reconstruction.shape)
            percent_loss = tf.math.multiply(tf.abs(data-reconstruction)/tf.add(data, 1e-3), 1e-1)
            #tf.print(percent_loss)
            percent_loss = tf.reduce_mean(tf.reduce_sum(percent_loss, axis=1))
            #tf.print(percent_loss)
            classifier_bce = keras.losses.binary_crossentropy(labels, prediction, from_logits=False)
            total_loss = 100*classifier_bce + (reconstruction_loss + kl_loss) #+ percent_loss

        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        self.percent_loss_tracker.update_state(percent_loss)
        self.classifier_bce_tracker.update_state(classifier_bce)
        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
            "percent_loss": self.percent_loss_tracker.result(),
            "classifier_bce": self.classifier_bce_tracker.result(),
        }

    def predict(self, x, batch_size):
        _, _, encoded_x = self.encoder.predict(x, batch_size=batch_size)
        decoded_x = self.decoder.predict(encoded_x, batch_size=batch_size)
        return decoded_x

    def classify(self, x, batch_size):
        _, _, encoded_x = self.encoder.predict(x, batch_size=batch_size)
        prediction = self.classifier.predict(encoded_x, batch_size=batch_size)
        return prediction

    # def call(self, x):
    #     _, _, x = self.encoder(x)
    #     x = self.classifier(x)
    #     x = tf.math.round(x)
    #     print(x)
    #     return x


encoder = Encoder(latent_dim)
decoder = Decoder(latent_dim)
classifier = ClassifierNN(latent_dim)
cvae = CVAE(encoder, decoder, classifier)
cvae.compile(optimizer=keras.optimizers.Adam(lr=lr))
# cvae.encoder.build(input_shape=(None, original_dim, 1))
# cvae.encoder.summary()
# cvae.decoder.build(input_shape=(None, latent_dim, 1))
# cvae.decoder.summary()
# cvae.build(input_shape=(None, original_dim, 1))
# cvae.summary()
# cvae.encoder.build()
cvae.encoder.summary()
cvae.decoder.summary()
cvae.classifier.summary()

# encoder.build(input_shape=(None, original_dim, 1))
# encoder.summary()

# class CAE(keras.Model):
#     def __init__(self, latent_dim):
#         super(CAE, self).__init__()
#         self.latent_dim = latent_dim
#         self.encoder = keras.Sequential([
#             keras.layers.InputLayer(input_shape=(original_dim, 1)),
#             keras.layers.Conv1D(32, kernel_size=3, activation='relu', padding='valid', strides=2),
#             keras.layers.Conv1D(64, kernel_size=3, activation='relu', padding='valid', strides=2),
#             keras.layers.Flatten(),
#             keras.layers.Dense(latent_dim, activation='relu'),
#             keras.layers.Dense(latent_dim, name='z_mean'),
#             keras.layers.Dense(latent_dim, name='z_log_var'),
#
#         ])
#         self.decoder = keras.Sequential([
#             keras.layers.InputLayer(input_shape=(latent_dim, 1)),
#             keras.layers.Dense(units=latent_dim, activation=tf.nn.relu),
#             keras.layers.Conv1DTranspose(64, kernel_size=3, activation='relu', padding='same', strides=2),
#             keras.layers.Conv1DTranspose(32, kernel_size=3, activation='relu', padding='same', strides=2),
#             keras.layers.Flatten(),
#             keras.layers.Dense(original_dim),
#             keras.layers.Reshape(target_shape=(original_dim, 1)),
#             keras.layers.Conv1D(1, kernel_size=3, activation='sigmoid', padding='same')
#         ])
#
#     def call(self, x):
#         encoded = self.encoder(x)
#         decoded = self.decoder(encoded)
#         return decoded
#
#     @tf.function
#     def sample(self, eps=None):
#         if eps is None:
#             eps = tf.random.normal(shape=(100, self.latent_dim))
#         return self.decode(eps, apply_sigmoid=True)
#
#     def encode(self, x):
#         mean, logvar = tf.split(self.encoder(x), num_or_size_splits=2, axis=1)
#         return mean, logvar
#
#     def reparameterize(self, mean, logvar):
#         eps = tf.random.normal(shape=mean.shape)
#         return eps * tf.exp(logvar * .5) + mean
#
#     def decode(self, z, apply_sigmoid=False):
#         logits = self.decoder(z)
#         if apply_sigmoid:
#             probs = tf.sigmoid(logits)
#             return probs
#         return logits


#cae = CAE(latent_dim)
#cae.compile(optimizer='adam', loss=keras.losses.BinaryCrossentropy(from_logits=False))
cae = cvae
# cae.encoder.summary()
# cae.decoder.build(input_shape=(None, 13, 3))
# cae.decoder.summary()

#
# inputs = keras.Input(shape=(original_dim,))
# h = layers.Dense(intermediate_dim, activation='relu')(inputs)
# z_mean = layers.Dense(latent_dim)(h)
# z_log_sigma = layers.Dense(latent_dim)(h)
#
# from keras import backend as K
#
# def sampling(args):
#     z_mean, z_log_sigma = args
#     epsilon = K.random_normal(shape=(K.shape(z_mean)[0], latent_dim),
#                               mean=0., stddev=0.1)
#     return z_mean + K.exp(z_log_sigma) * epsilon
#
# z = layers.Lambda(sampling)([z_mean, z_log_sigma])
#
# # Create encoder
# encoder = keras.Model(inputs, [z_mean, z_log_sigma, z], name='encoder')
#
# # Create decoder
# latent_inputs = keras.Input(shape=(latent_dim,), name='z_sampling')
# x = layers.Dense(intermediate_dim, activation='relu')(latent_inputs)
# outputs = layers.Dense(original_dim, activation='sigmoid')(x)
# decoder = keras.Model(latent_inputs, outputs, name='decoder')
#
# # instantiate VAE model
# outputs = decoder(encoder(inputs)[2])
# vae = keras.Model(inputs, outputs, name='vae_mlp')
#
# reconstruction_loss = keras.losses.binary_crossentropy(inputs, outputs)
# reconstruction_loss *= original_dim
# kl_loss = 1 + z_log_sigma - K.square(z_mean) - K.exp(z_log_sigma)
# kl_loss = K.sum(kl_loss, axis=-1)
# kl_loss *= -0.5
# vae_loss = K.mean(reconstruction_loss + kl_loss)
# vae.add_loss(vae_loss)
# vae.compile(optimizer='adam')

from keras.backend import expand_dims as expand_dims
cutoff = no_seizure.shape[0] - seizure.shape[0]
(x_train, y_train), (x_test, y_test) = (no_seizure[0:cutoff], labels[labels == 0][0:cutoff]), (np.concatenate((no_seizure[cutoff:], seizure)), np.concatenate((labels[labels==0][cutoff:], labels[labels == 1])))
(x_train, y_train), (x_test, y_test) = (expand_dims(x_train, axis=-1), expand_dims(y_train, axis=-1)), (expand_dims(x_test, axis=-1), y_test) #expand_dims(y_test, axis=-1))
# cutoff = int(no_seizure.shape[0]*0.9)
# (x_train, y_train), (x_test, y_test) = (no_seizure[0:cutoff], labels[labels == 0][0:cutoff]), (np.concatenate((no_seizure[cutoff:], seizure)), np.concatenate((labels[labels==0][cutoff:], labels[labels == 1])))
# (x_train, y_train), (x_test, y_test) = (no_seizure[0::2], labels[labels == 0][0::2]), (np.concatenate((no_seizure[1::2], seizure)), np.concatenate((labels[labels==0][1::2], labels[labels == 1])))

## For NN classifier
# cutoff = no_seizure.shape[0] - seizure.shape[0]
# (x_train, y_train), (x_test, y_test) = (epochs[0:cutoff], labels[0:cutoff]), (epochs[cutoff:], labels[cutoff:])
# (x_train, y_train), (x_test, y_test) = (expand_dims(x_train, axis=-1), expand_dims(y_train, axis=-1)), (
# expand_dims(x_test, axis=-1), y_test)

x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(epochs, labels, test_size=0.2, stratify=labels)

pds = tf.data.Dataset.from_tensor_slices((expand_dims(x_train[y_train == 1], axis=-1), expand_dims(y_train[y_train == 1], axis=-1)))
pds = pds.shuffle(100000).repeat()
nds = tf.data.Dataset.from_tensor_slices((expand_dims(x_train[y_train == 0], axis=-1), expand_dims(y_train[y_train == 0], axis=-1)))
nds = nds.shuffle(100000).repeat()
resampled_ds = tf.data.Dataset.sample_from_datasets([pds, nds], weights=[0.5, 0.5])
resampled_ds = resampled_ds.batch(32).prefetch(2)
spe = np.ceil(2.0*no_seizure.shape[0]/32)

train_ds = resampled_ds

# test_ds = test_ds.take(epochs.shape[0]-int(0.8*epochs.shape[0]))


print(x_train.shape, y_train.shape, x_test.shape, y_test.shape) ## should be something like (2703, 52, 1) (2703, 1) (226, 52, 1) (226,)

# from tensorflow.python.ops.numpy_ops import np_config
# np_config.enable_numpy_behavior()
# x_train = x_train.astype('float32') / 255.
# x_test = x_test.astype('float32') / 255.
# x_train = x_train.reshape((1, len(x_train), np.prod(x_train.shape[1:])))
# x_test = x_test.reshape((1, len(x_test), np.prod(x_test.shape[1:])))

# cae.fit(x_train, y_train,
#         epochs=n_epochs,
#         batch_size=batch_size)
cae.fit(train_ds,
        steps_per_epoch=spe,
        epochs=n_epochs,
        batch_size=batch_size)


#x_test_encoded = encoder.predict(x_test, batch_size=32)
#x_test_decoded = decoder.predict(x_test_encoded, batch_size=32)

## From https://www.tensorflow.org/tutorials/generative/autoencoder
oldplots = False
if oldplots:
    x_test2 = np.squeeze(x_test)
    x_train2 = np.squeeze(x_train)
    reconstructions = np.squeeze(cae.predict(x_test, batch_size=32))
    train_reconstructions = np.squeeze(cae.predict(x_train, batch_size=32))
    recloss = keras.losses.binary_crossentropy(reconstructions, x_test2)

    print(reconstructions.shape)
    trainloss = keras.losses.binary_crossentropy(train_reconstructions, x_train2)
    print(recloss.shape)
    print(x_test.shape)
    plotdata = np.squeeze(
        np.array([recloss.numpy()[y_test == 0][None, None, :], recloss.numpy()[y_test == 1][None, None, :]]))
    print(plotdata.shape)
    plt.hist(plotdata.T, bins=50, stacked=True, color=["blue", "red"], label=['Non-seizure', 'Seizure'])

    plt.xlabel("Test loss")
    plt.ylabel("No of examples")
    # plt.title("{intermediate_dim} {latent_dim} {batch_size}".format(intermediate_dim, latent_dim, batch_size))
    plt.show()

    std_ratio = 1
    threshold = np.mean(trainloss) + (np.std(trainloss) * std_ratio)
    # thresholdratio = 1
    # threshold *= thresholdratio
    print("Threshold: ", threshold, " with std dev ratio:", std_ratio)
    pred_labels = recloss >= threshold
    correct = np.sum(pred_labels == y_test)
    acc = correct / y_test.shape[0]
    print(acc)

    pred_labels = pred_labels.numpy()

    tp = pred_labels[pred_labels == y_test]
    tp = np.sum(tp == 1)
    fp = pred_labels[pred_labels != y_test]
    fp = np.sum(fp == 1)
    tn = pred_labels[pred_labels == y_test]
    tn = np.sum(tn == 0)
    fn = pred_labels[pred_labels != y_test]
    fn = np.sum(fn == 0)
    tpr = tp / (tp + fn)
    tnr = tn / (tn + fp)
    fpr = fp / (fp + tn)
    fnr = fn / (fn + tp)

    print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
    print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)

    ## try sorting to see if the reconstruction error is highest during seizures
    maxes_i = np.argpartition(recloss, -1 * seizure.shape[0])[-1 * seizure.shape[0]:]
    pred_labels = np.zeros_like(y_test)
    pred_labels[maxes_i] = 1
    correct = np.sum(pred_labels == y_test)
    acc = correct / y_test.shape[0]
    print(acc)
    tp = pred_labels[pred_labels == y_test]
    tp = np.sum(tp == 1)
    fp = pred_labels[pred_labels != y_test]
    fp = np.sum(fp == 1)
    tn = pred_labels[pred_labels == y_test]
    tn = np.sum(tn == 0)
    fn = pred_labels[pred_labels != y_test]
    fn = np.sum(fn == 0)
    tpr = tp / (tp + fn)
    tnr = tn / (tn + fp)
    fpr = fp / (fp + tn)
    fnr = fn / (fn + tp)

    print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
    print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)

    pick = 101
    decoded_data = np.squeeze(cae.predict(x_test, batch_size=32)[pick])
    plt.plot(x_test2[pick], 'b')
    plt.plot(decoded_data, 'r')
    plt.fill_between(np.arange(original_dim), decoded_data, x_test2[pick], color='lightcoral')
    plt.legend(labels=["Input", "Reconstruction", "Error"])
    plt.show()

    pick = 201
    decoded_data = np.squeeze(cae.predict(x_test, batch_size=32)[pick])
    plt.plot(x_test2[pick], 'b')
    plt.plot(decoded_data, 'r')
    plt.fill_between(np.arange(original_dim), decoded_data, x_test2[pick], color='lightcoral')
    plt.legend(labels=["Input", "Reconstruction", "Error"])
    plt.show()

    print(x_test2[pick])

    x_test2 = np.squeeze(x_test)
    x_train2 = np.squeeze(x_train)
    reconstructions = np.squeeze(cae.predict(x_test, batch_size=32))
    train_reconstructions = np.squeeze(cae.predict(x_train, batch_size=32))

    recloss = keras.losses.binary_crossentropy(reconstructions, x_test2)
    ploss = np.sum(np.abs(x_test2-reconstructions)/(x_test2 + 1e-12))

    trainloss = keras.losses.binary_crossentropy(train_reconstructions, x_train2)
    trainploss = np.sum(np.abs(x_train2-train_reconstructions)/(x_train2 + 1e-12))

    recloss = recloss.numpy()
    recloss *= ploss
    trainloss = trainloss.numpy()
    trainloss *= trainploss

    plotdata = np.squeeze(
        np.array([recloss[y_test == 0][None, None, :], recloss[y_test == 1][None, None, :]]))
    print(plotdata.shape)
    plt.hist(plotdata.T, bins=50, stacked=True, color=["blue", "red"], label=['Non-seizure', 'Seizure'])
    plt.xlabel("Test loss")
    plt.ylabel("No of examples")
    plt.show()


# resampled_results = cae.evaluate(test_ds, batch_size=32, verbose=0)
# for name, value in zip(cae.metrics_names, resampled_sults):
# #     print(name, ': ', value)re
# print()


# predictions = np.array([])
# y_test = np.array([])
# for features, labels in test_ds:
#     predictions = np.append(predictions, cae.classify(features, batch_size=32))
#     y_test = np.append(y_test, labels)
# y_test = np.squeeze(y_test.flatten())
# predictions = np.squeeze(predictions.flatten())
# print(predictions)
# print(predictions.shape)
# pred_labels = np.rint(predictions)

predictions = cae.classify(np.expand_dims(x_test, axis=-1), batch_size=32)
predictions = np.squeeze(predictions.flatten())
pred_labels = np.rint(predictions)

correct = np.sum(pred_labels == y_test)
acc = correct / y_test.shape[0]
print(acc)

tp = pred_labels[pred_labels == y_test]
tp = np.sum(tp == 1)
fp = pred_labels[pred_labels != y_test]
fp = np.sum(fp == 1)
tn = pred_labels[pred_labels == y_test]
tn = np.sum(tn == 0)
fn = pred_labels[pred_labels != y_test]
fn = np.sum(fn == 0)
tpr = tp / (tp + fn)
tnr = tn / (tn + fp)
fpr = fp / (fp + tn)
fnr = fn / (fn + tp)

print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)


#
# pred_labels = cae.classify(test_ds, batch_size=32)
# print(np.squeeze(pred_labels))
# pred_labels = np.squeeze(np.rint(pred_labels))


# print("NN classifier:")
# x, y = test_ds.take(176*32)
# y_test = y.numpy()
# print(y)
# predictions = cvae.classify(x, batch_size=32)
# pred_labels = np.squeeze(np.rint(predictions))
# print(np.squeeze(predictions))
# print(y_test)
#
# correct = np.sum(pred_labels == y_test)
# acc = correct / y_test.shape[0]
# print(acc)
#
#
# tp = pred_labels[pred_labels == y_test]
# tp = np.sum(tp == 1)
# fp = pred_labels[pred_labels != y_test]
# fp = np.sum(fp == 1)
# tn = pred_labels[pred_labels == y_test]
# tn = np.sum(tn == 0)
# fn = pred_labels[pred_labels != y_test]
# fn = np.sum(fn == 0)
# tpr = tp / (tp + fn)
# tnr = tn / (tn + fp)
# fpr = fp / (fp + tn)
# fnr = fn / (fn + tp)
#
# print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
# print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)

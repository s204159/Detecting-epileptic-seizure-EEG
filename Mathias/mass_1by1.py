import numpy as np
import mne ## Handles EEG data
import matplotlib.pyplot as plt
from glob import glob
import re
from helper import read_fifs

files = ["data/out/chb{:02d}".format(x) for x in np.arange(1, 25)]

kwargs = dict(fmin=0.5, fmax=49.75, n_jobs=1)
picks = ["FP1-F7"]
tmin = 0.0
baseline = None

patientsArray = []
edfsArray = []
eventsArray = []
labelsArray = []
meansArray = []
mediansArray = []


for i_patient, file in enumerate(files):
    edfs = read_fifs(file)

    for i_edf, data in enumerate(edfs):
        if len(data.annotations) == 0:
            continue

        epochsArray = []
        onsets = data.annotations.onset
        durations = data.annotations.duration
        events, event_id = mne.events_from_annotations(data)
        N_seizures = len(data.annotations)

        for i_event, event in enumerate(events):
            tmax = durations[i_event]

            epochSeizure = mne.Epochs(data, [event], event_id, tmin, tmax, picks=picks, baseline=baseline, preload=True)
            patientsArray.append(i_patient + 1)
            edfsArray.append(i_edf + 1)
            eventsArray.append(i_event + 1)
            labelsArray.append(1)
            epochsArray.append(epochSeizure)
            ## if seizure is in second half of signal, sample from the first half
            if onsets[i_event] + durations[i_event] >= len(data) / (256 * 2):
                start = np.random.randint(0, (len(data) / (256 * 2))-durations[i_event])
                epochNormal = mne.Epochs(data, np.array([[start, 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax,
                                         baseline=baseline, preload=True)
            ## if seizure is in first half of signal, sample from the second half
            else:
                start = np.random.randint(len(data) / (256 * 2), (len(data) / 256)-durations[i_event])
                epochNormal = mne.Epochs(data, np.array([[start, 0, 2]]), {'Normal': 2}, tmin=tmin, tmax=tmax,
                                         baseline=baseline, preload=True)
            patientsArray.append(i_patient + 1)
            edfsArray.append(i_edf + 1)
            eventsArray.append(0)
            labelsArray.append(0)
            epochsArray.append(epochNormal)


        for epoch in epochsArray:
            psds_welch_mean, freqs_mean = mne.time_frequency.psd_welch(epoch, average='mean', **kwargs)
            psds_welch_median, freqs_median = mne.time_frequency.psd_welch(epoch, average='median', **kwargs)

            # Convert power to dB scale.
            psds_welch_mean = 10 * np.log10(psds_welch_mean)
            psds_welch_median = 10 * np.log10(psds_welch_median)

            meansArray.append(psds_welch_mean)
            mediansArray.append(psds_welch_median)



f = open("patients.txt", "w")
f.write(str(patientsArray))
f.close()

f = open("edfs.txt", "w")
f.write(str(edfsArray))
f.close()

f = open("events.txt", "w")
f.write(str(eventsArray))
f.close()

f = open("labels.txt", "w")
f.write(str(labelsArray))
f.close()

f = open("means.txt", "w")
f.write(str(meansArray))
f.close()

f = open("medians.txt", "w")
f.write(str(mediansArray))
f.close()



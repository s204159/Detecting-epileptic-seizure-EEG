import re
from glob import glob

from scipy import signal
import numpy as np
from tqdm.auto import tqdm
import mne

mne.set_log_level('Warning')
import datetime

# data holders
epochs = []
epochs_patient_id = []
epochs_recording_session = []
epochs_type = []

# parameters
padding_hours = 1
epoch_length = 4  # measured in seconds
sample_rate_hz = 256  # measured in hz
preictal_state_length = 30 * 60  # measured in seconds
interictal_state_padding = 4 * 60 * 60  # measured in seconds, interictal state should be this away from a seizure.

# Gets patients
patient_numbers = [i for i in range(1, 2)]


# patient_numbers.remove(21) # same patient 2 years later. Removed to keep patients independent of each other.
def get_next_seizure_time(lines, i, current_time, current_day):
    while i < len(lines):
        # continues to next edf file
        if lines[i][:10] != 'File Name:':
            i += 1
            continue

        number_seizures = int(lines[i + 3][-1])

        for k in range(number_seizures):
            file_start_time = lines[i + 1][-8:]
            file_end_time = lines[i + 2][-8:]
            onset = int(re.findall('\d+', lines[i + 4 + k * 2])[-1])
            end_time = int(re.findall('\d+', lines[i + 5 + k * 2])[-1])

            edf_start_time = datetime.datetime(2000, 1, 1, int(file_start_time[:2]), int(file_start_time[3:5]),
                                               int(file_start_time[6:8]), 0)
            edf_end_time = datetime.datetime(2000, 1, 1, int(file_end_time[:2]), int(file_end_time[3:5]),
                                             int(file_end_time[6:8]), 0)
            if (edf_end_time - current_time).total_seconds() + current_time.day - edf_end_time.day * 24 * 60 * 60 < 0:
                edf_start_time = datetime.datetime(2000, 1, current_time.day + 1, int(file_start_time[:2]),
                                                   int(file_start_time[3:5]), int(file_start_time[6:8]),
                                                   0)  # THIS ONLY WORKS IF CURRRENT DAY ROLLS OVER 24:00 IF NOT WE NEED TO ADD 1 TO CURRENT DAY

            # if current_time.day + 1 == 3:
            #     print('sadf')
            next_seizure_time = edf_start_time + datetime.timedelta(seconds=onset)
            next_seizure_time_end_time = edf_start_time + datetime.timedelta(seconds=end_time)

            if (next_seizure_time_end_time - current_time).total_seconds() >= epoch_length:
                return next_seizure_time, edf_start_time + datetime.timedelta(seconds=end_time)
            else:
                continue

        i += 1
    # no new seizures in edf recordings
    return None, None


for patient_number in tqdm(patient_numbers):
    path_to_folder = f'../../chbmit/chb{patient_number:02d}'
    summary_path = glob(path_to_folder + '/*summary.txt')

    prev_seizure_time = datetime.datetime(1, 1, 1, 1, 1, 1)
    current_time = None
    next_seizure_end_time = None
    file_start_time = None
    prev_file_start_time = None
    current_day = 1
    prev_next_seizure_end_time = None

    with open(summary_path[0]) as file:
        lines = [line.rstrip() for line in file]
        i = 0

        while i < len(lines):
            # continues to next edf file
            if lines[i][:10] != 'File Name:':  # or lines[i] != 'File Name: chb01_04.edf' :
                i += 1
                continue

            # Gets info about the current edf file
            edf_file_path = path_to_folder + '/' + lines[i][11:]

            if file_start_time is not None:
                prev_file_start_time = file_start_time
            file_start_time = lines[i + 1][-8:]

            file_end_time = lines[i + 2][-8:]
            number_seizures = int(lines[i + 3][-1])

            if prev_file_start_time and (
                    datetime.datetime(2000, 1, 1, int(prev_file_start_time[:2]), int(prev_file_start_time[3:5]),
                                      int(prev_file_start_time[6:8]), 0) - datetime.datetime(2000, 1, current_day,
                                                                                             int(file_start_time[:2]),
                                                                                             int(file_start_time[3:5]),
                                                                                             int(file_start_time[6:8]),
                                                                                             0)).total_seconds() > 0:
                # new day
                current_day += 1

            # sets the current time
            # if current_time is None:
            current_time = datetime.datetime(2000, 1, current_day, int(file_start_time[:2]), int(file_start_time[3:5]),
                                             int(file_start_time[6:8]), 0)
            # else:  #
            #    edf_time = datetime.datetime(2000, 1, 1, int(file_start_time[:2]), int(file_start_time[3:5]),
            #                                 int(file_start_time[6:8]), 0)
            #    if (edf_time - current_time).total_seconds() > 0:
            #        current_time = edf_time
            #    else:
            #        # rolls to next day
            #        current_time = datetime.datetime(2000, 1, current_time.day + 1, int(file_start_time[:2]),
            #                                         int(file_start_time[3:5]), int(file_start_time[6:8]), 0)

            # Loads edf file into mne
            edf = mne.io.read_raw_edf(edf_file_path, preload=True, verbose='ERROR')
            # edf.set_eeg_reference()

            # skips if the electrodes we use does not exist
            if 'FP1-F7' not in edf.info['ch_names']:
                i += 1
                continue

            edf = edf.pick_channels(['FP1-F7', ])
            edf_data = edf.get_data()

            edf_splits_length = epoch_length * sample_rate_hz
            edf_splits = [edf_data[:, x:x + edf_splits_length] for x in range(0, edf_data.shape[1], edf_splits_length)
                          if x + edf_splits_length <= edf_data.shape[1]]

            for edf_split in edf_splits:
                if next_seizure_end_time is not None:
                    prev_next_seizure_end_time = next_seizure_end_time
                next_seizure_start_time, next_seizure_end_time = get_next_seizure_time(lines, i, current_time,
                                                                                       current_day)
                if prev_next_seizure_end_time is not None and prev_next_seizure_end_time != next_seizure_end_time:
                    prev_seizure_time = prev_next_seizure_end_time
                    print('updated time')

                time_to_prev_seizure = (current_time - prev_seizure_time).total_seconds()

                if time_to_prev_seizure > 60 * 60 * padding_hours and (next_seizure_start_time is None or (
                        next_seizure_start_time - current_time).total_seconds() > 60 * 60 * padding_hours):
                    epochs.append(edf_split)
                    epochs_patient_id.append(patient_number)
                    epochs_recording_session.append(lines[i][11:])
                    epochs_type.append(0)
                elif next_seizure_start_time and 0 < (next_seizure_start_time - current_time).total_seconds() and (
                        next_seizure_start_time - current_time).total_seconds() <= 60 * 30:
                    epochs.append(edf_split)
                    epochs_patient_id.append(patient_number)
                    epochs_recording_session.append(lines[i][11:])
                    epochs_type.append(1)

                if next_seizure_start_time and (
                        (next_seizure_end_time - current_time).total_seconds() >= epoch_length) and (
                        (next_seizure_start_time - current_time).total_seconds() <= 0):
                    epochs.append(edf_split)
                    epochs_patient_id.append(patient_number)
                    epochs_recording_session.append(lines[i][11:])
                    epochs_type.append(2)

                current_time = current_time + datetime.timedelta(seconds=epoch_length)

            i += 1
            # END WHILE LOOP

print('done')

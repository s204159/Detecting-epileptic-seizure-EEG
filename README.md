# Detecting the presence of an epileptic seizure using a single-channel EEG
02466 Project work - Bachelor of Artificial Intelligence and Data Spring 22 - Technical University of Denmark

By Mathias Daniel Frosz Nielsen (s201968) and Christian Kento Rasmussen (s204159)


## Installation
1. Clone project from gitlab.
2. Install Tensorflow and its dependencies.
3. As an example run the CVAE model in [CVAEModel.py](EEGEpilepticDetection/CVAEModel.py)

## Generel usage
This repo contains three main packages. The packages 'Christian' and 'Mathias' contains temporary files and helper functions used during development. 

The main package 'EEGEpilepticDetection' contains the python scripts used to train and evaluate the models describes in the Project work report.
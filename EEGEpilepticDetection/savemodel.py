from CVAEModel import CVAE, Encoder, Decoder, ClassifierNN
from NNEpilepticModel import CNNModel
from dataSegmener import DataSegmenter
from tensorflow import keras as keras
from statsmodels.stats.contingency_tables import mcnemar
import tensorflow as tf
from tensorflow import keras as keras

import numpy as np
import random
import tensorflow as tf

## Set random seed, still some randomness due to using GPU
rs = 42
random.seed(rs)
np.random.seed(rs)

## Set parameters
sampling_rate = 256
full_time_series = False
is_testing = False
n_epochs = 5
patient_start = 1
n_patients = 1
lr = 5e-4


original_dim = 52
intermediate_dim = 8
latent_dim = 13
batch_size = 32

reader = tf.train.load_checkpoint("./weightsCVAE/seiznonseiz_e5_testp1_fp1fp2/")
shape_from_key = reader.get_variable_to_shape_map()
dtype_from_key = reader.get_variable_to_dtype_map()

print(sorted(shape_from_key.keys()))

# encoder = Encoder(latent_dim)
# decoder = Decoder(latent_dim)
# classifier = ClassifierNN(latent_dim)
# cvae = CVAE(encoder, decoder, classifier)
# cvae.compile(optimizer=keras.optimizers.Adam(lr=lr))
# cvae.build(input_shape=(1, 52, 1))
# cvae(np.zeros((1, 52, 1)))
# keras.models.save_model(cvae, "savedmodel/1/")

# cnn = CNNModel()
# cnn.build()
# cnn(np.zeros((1, 52, 1)))
# cnn.compile(optimizer=tf.keras.optimizers.Adam(),
#             loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
#             metrics=['accuracy'])
# keras.models.save_model(cnn, "savedmodelCNN/1/")

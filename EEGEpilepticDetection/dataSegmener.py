import re
import datetime
import mne
import numpy as np
import scipy.signal as signal

from glob import glob
from tqdm.auto import tqdm

mne.set_log_level('Warning')

import numpy.random as random


class EdfContainer:
    def __init__(self, start_day, start_time, data, edf_file_path, number_of_seizures_in_edf, edf_seizures_start_times,
                 edf_seizures_end_times):
        self.start_day = start_day
        self.start_time = start_time
        self.data = data
        self.edf_file_path = edf_file_path
        self.edf_seizures_start_times = edf_seizures_start_times
        self.edf_seizures_end_times = edf_seizures_end_times
        self.number_of_seizures_in_edf = number_of_seizures_in_edf

    def __repr__(self):
        return self.edf_file_path + " | " + self.start_time.__repr__()

    def __str__(self):
        return self.edf_file_path + " | " + self.start_time.__repr__()


class DataSegmenter():


    def __init__(self, path_to_data="../../chbmit"):
        self.patient_numbers = None
        self.path_to_data = path_to_data

        # data holders
        self.epochs = []
        self.epochs_patient_id = []
        self.epochs_recording_session = []
        self.epochs_type = []

        # parameters
        self.padding_hours = 1
        self.epoch_length = 4  # measured in seconds
        self.sample_rate_hz = 256  # measured in hz
        self.preictal_state_length = 30 * 60  # measured in seconds
        self.interictal_state_padding = 4 * 60 * 60  # measured in seconds, interictal state should be this away from a seizure.
        self.default_year = 2000
        self.default_month = 1
        self.default_day = 1

    def get_segment_type(self, i, current_time):
        EdfSegment = self.EdfSegments[i]

        for k in range(EdfSegment.number_of_seizures_in_edf):
            start = EdfSegment.edf_seizures_start_times[k]
            end = EdfSegment.edf_seizures_end_times[k]

            # Checks if we are in a seizure
            if (current_time - start).total_seconds() > 0 and (current_time - end).total_seconds() <= 0:
                return 0

            # checks if we are in pre-ictal fase
            if (start - current_time).total_seconds() >= self.preictal_state_length:
                return 1

        # finds time to last seizure and next seizure to make sure we are outside interval
        last_seizure_end_time = None
        for EdfSegment in reversed(self.EdfSegments[:i]):
            for k in reversed(range(EdfSegment.number_of_seizures_in_edf)):
                end = EdfSegment.edf_seizures_end_times[k]

                if (current_time - end).total_seconds() < 0:
                    last_seizure_end_time = end

        next_seizure_start_time = None
        for EdfSegment in self.EdfSegments[i:]:
            for k in range(EdfSegment.number_of_seizures_in_edf):
                start = EdfSegment.edf_seizures_start_times[k]

                if (current_time - start).total_seconds() > 0:
                    next_seizure_start_time = start

        # calculates if we are in window
        if (next_seizure_start_time == None or (
                next_seizure_start_time - current_time).total_seconds() > self.interictal_state_padding) and (
                last_seizure_end_time == None or (
                current_time - last_seizure_end_time).total_seconds() > self.interictal_state_padding):
            return 2

        return 3

    def import_data(self, patient_numbers=None, downsample=[1, 1, 1, 1]):
        # Gets patients
        if patient_numbers is None:
            self.patient_numbers = [i for i in range(1, 3)]
        else:
            self.patient_numbers = patient_numbers

        for patient_number in tqdm(self.patient_numbers):
            path_to_folder = self.path_to_data + f'/chb{patient_number:02d}'
            summary_path = glob(path_to_folder + '/*summary.txt')

            self.EdfSegments = []
            start_day = self.default_day

            with open(summary_path[0]) as file:
                lines = [line.rstrip() for line in file]
                i = 0

                while i < len(lines):
                    # continues to next edf file
                    if lines[i][:10] != 'File Name:':
                        i += 1
                        continue

                    edf_file_path = path_to_folder + '/' + lines[i][11:]
                    file_start_time = lines[i + 1][-8:]

                    file_end_time = lines[i + 2][-8:]
                    edf = mne.io.read_raw_edf(edf_file_path, preload=True, verbose='ERROR')
                    number_of_seizures_in_edf = int(lines[i + 3][-1])

                    edf_start_time = datetime.datetime(self.default_year, self.default_month, start_day,
                                                       int(file_start_time[:2]),
                                                       int(file_start_time[3:5]),
                                                       int(file_start_time[6:8]), 0)

                    # Sets the start time with correct day
                    if len(self.EdfSegments) != 0:
                        if (edf_start_time - self.EdfSegments[-1].start_time).total_seconds() < 0:
                            start_day += 1

                            edf_start_time = datetime.datetime(self.default_year, self.default_month, start_day,
                                                               int(file_start_time[:2]),
                                                               int(file_start_time[3:5]),
                                                               int(file_start_time[6:8]), 0)

                    # skips if the electrodes we use does not exist
                    if np.any([channel not in edf.info['ch_names'] for channel in
                               ['FP1-F7', 'F7-T7', 'T7-FT9', 'FT9-FT10', 'FT10-T8', 'F8-T8', 'FP2-F8']]):
                        i += 1
                        print('Skipped because of missing channels')
                        continue

                    edf = edf.pick_channels(['FP1-F7', 'F7-T7', 'T7-FT9', 'FT9-FT10', 'FT10-T8', 'F8-T8', 'FP2-F8'])
                    fp1f7 = edf.get_data(['FP1-F7'])
                    f7t7 = edf.get_data(['F7-T7'])
                    t7ft9 = edf.get_data(['T7-FT9'])
                    ft9ft10 = edf.get_data(['FT9-FT10'])
                    ft10t8 = edf.get_data(['FT10-T8'])
                    f8t8 = edf.get_data(['F8-T8'])
                    fp2f8 = edf.get_data(['FP2-F8'])
                    ## Formula for reconstructing FP1-FP2 signal in CHB-MIT dataset from Yanjun at Curvex
                    edf_data = fp1f7 + f7t7 + t7ft9 + ft9ft10 + ft10t8 - f8t8 - fp2f8

                    # apply cutoff filter to data (4th order butterworth between 0.5 and 49.75 hz)
                    edf_data = mne.filter.filter_data(edf_data, sfreq=self.sample_rate_hz, l_freq=0.5, h_freq=49.75,
                                                      method="iir",
                                                      verbose=False)

                    edf_seizures_start_times = []
                    edf_seizures_end_times = []

                    for k in range(number_of_seizures_in_edf):
                        onset = int(re.findall('\d+', lines[i + 4 + k * 2])[-1])
                        end_time = int(re.findall('\d+', lines[i + 5 + k * 2])[-1])

                        next_seizure_time = edf_start_time + datetime.timedelta(seconds=onset)
                        edf_seizures_start_times.append(next_seizure_time)
                        next_seizure_time_end_time = edf_start_time + datetime.timedelta(seconds=end_time)
                        edf_seizures_end_times.append(next_seizure_time_end_time)

                    self.EdfSegments.append(EdfContainer(start_day=start_day,
                                                         start_time=edf_start_time,
                                                         data=edf_data,
                                                         edf_file_path=edf_file_path,
                                                         number_of_seizures_in_edf=number_of_seizures_in_edf,
                                                         edf_seizures_start_times=edf_seizures_start_times,
                                                         edf_seizures_end_times=edf_seizures_end_times))
                    i += 1

            for i, EdfSegment in enumerate(self.EdfSegments):
                # if i != 2:
                #    continue

                edf_data = EdfSegment.data

                edf_splits_length = self.epoch_length * self.sample_rate_hz
                edf_splits = [edf_data[:, x:x + edf_splits_length] for x in
                              range(0, edf_data.shape[1], edf_splits_length)
                              if x + edf_splits_length <= edf_data.shape[1]]

                current_time = EdfSegment.start_time

                for edf_split in edf_splits:
                    type = self.get_segment_type(i, current_time)

                    if random.uniform(0, 1) < downsample[type]:  # used for down sampling
                        self.epochs.append(edf_split)
                        self.epochs_patient_id.append(patient_number)
                        # self.epochs_recording_session.append(f"{patient_number}, {i}")
                        self.epochs_recording_session.append(i)
                        self.epochs_type.append(type)

                    current_time += datetime.timedelta(seconds=self.epoch_length)

    def get_data(self, full_time_series=True, upsample=True, positive_class=[0], negative_class=[2]):

        if not full_time_series:
            epochsArray = np.moveaxis(np.array(self.epochs), 1, 2)
            mediansArray = []

            for epoch in epochsArray:
                freqs_median, psds_welch_median = signal.welch(epoch[:, 0], fs=self.sample_rate_hz, nperseg=256,
                                                               average='median')
                psds_welch_median = np.append(psds_welch_median, 0)  # zero-padding
                mediansArray.append(psds_welch_median[0:52])
            epochs = np.array(mediansArray)

            # z-score normalization
            epochs = np.array([[(d - np.mean(l)) / (np.std(l) + (1e-30)) for d in l] for l in epochs])
            # normalize between 0 and 1 (swaps amplitude for percentual amplitude (why?))
            epochs = np.array([[(d - np.min(l)) / (np.max(l) - np.min(l) + (1e-30)) for d in l] for l in epochs])
            # convert to float32 for use in tensorflow
            epochs = epochs.astype("float32")

            epochs = np.array(epochs[..., np.newaxis])
        else:
            epochs = np.moveaxis(np.array(self.epochs), 1, 2)

        if upsample:
            pos_features = epochs[[epoch_type in positive_class for epoch_type in self.epochs_type]]
            neg_features = epochs[[epoch_type in negative_class for epoch_type in self.epochs_type]]

            pos_labels = np.full(len(pos_features), 1, dtype=int)  # epochs_type[(epochs_type == 0)]
            neg_labels = np.full(len(neg_features), 0, dtype=int)  # epochs_type[(epochs_type == 2)]

            ids = np.arange(len(pos_features))
            choices = np.random.choice(ids, len(neg_features))

            res_pos_features = pos_features[choices]
            res_pos_labels = pos_labels[choices]

            print(res_pos_features.shape)

            resampled_features = np.concatenate([res_pos_features, neg_features], axis=0)
            resampled_labels = np.concatenate([res_pos_labels, neg_labels], axis=0)

            order = np.arange(len(resampled_labels))
            np.random.shuffle(order)
            resampled_features = resampled_features[order]
            resampled_labels = resampled_labels[order]
            print(resampled_features.shape)
            return resampled_features, resampled_labels, None, None

        return epochs, np.array(self.epochs_type), np.array(self.epochs_patient_id), np.array(
            self.epochs_recording_session)

    def smart_loader(self, path_to_folder, patient_numbers=None, downsample=[1, 1, 1, 1], full_time_series=True,
                     upsample=True, positive_class=[0], negative_class=[2]):
        path_to_epochs = path_to_folder + f"epochs_{str(patient_numbers).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"
        path_to_epochs_type = path_to_folder + f"epochs_type_{str(patient_numbers).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"


        try:
            return np.load(path_to_epochs), np.load(path_to_epochs_type)
        except Exception:
            self.import_data(patient_numbers=patient_numbers, downsample=downsample)
            epochs, epochs_type, _, _ = self.get_data(full_time_series=full_time_series, upsample=upsample,
                                                      positive_class=positive_class, negative_class=negative_class)
            np.save(path_to_epochs, epochs)
            np.save(path_to_epochs_type, epochs_type)

            return epochs, epochs_type


    def smart_saver(self, path_to_folder, patient_numbers=None, downsample=[1, 1, 1, 1], full_time_series=True,
                     upsample=True, positive_class=[0], negative_class=[2]):
        path_to_epochs = path_to_folder + f"epochs_{str(patient_numbers).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"
        path_to_epochs_type = path_to_folder + f"epochs_type_{str(patient_numbers).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"


        try:
            return np.load(path_to_epochs), np.load(path_to_epochs_type)
        except Exception:
            self.import_data(patient_numbers=patient_numbers, downsample=downsample)
            epochs, epochs_type, _, _ = self.get_data(full_time_series=full_time_series, upsample=upsample,
                                                      positive_class=positive_class, negative_class=negative_class)
            np.save(path_to_epochs, epochs)
            np.save(path_to_epochs_type, epochs_type)

            return epochs, epochs_type

    # def smarter_loader(self, path_to_folder, patient_numbers=None, downsample=[1, 1, 1, 1], full_time_series=True,
    #                  upsample=True, positive_class=[0], negative_class=[2]):
    #     path_to_epochs = path_to_folder + f"epochs_{str(patient_numbers).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"
    #     path_to_epochs_type = path_to_folder + f"epochs_type_{str(patient_numbers).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"
    #
    #     self.epochs = np.concatenate((self.epochs, np.load(path_to_epochs)), axis=0)
    #     self.epochs_type = np.concatenate(self.epochs_type, (np.load(path_to_epochs_type)), axis=0)
    #

    def smarter_loader(self, path_to_folder, patient_numbers=None, downsample=[1, 1, 1, 1], full_time_series=True,
                       upsample=True, positive_class=[0], negative_class=[2]):
        k = 1

        for patient in patient_numbers:
            path_to_epochs = path_to_folder + f"epochs_{str([patient]).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"
            path_to_epochs_type = path_to_folder + f"epochs_type_{str([patient]).strip()}_{str(downsample).strip()}_{str(positive_class).strip()}_{str(negative_class).strip()}.npy"
            epochs_new, epochs_type_new = np.load(path_to_epochs), np.load(path_to_epochs_type)
            if k == 1:
                epochs = epochs_new.copy()
                epochs_type = epochs_type_new.copy()
                k = 0
            else:
                epochs = np.concatenate((epochs, epochs_new), axis=0)
                epochs_type = np.concatenate((epochs_type, epochs_type_new), axis=0)


        return epochs, epochs_type

    def smarter_loader_session(self, patient_number=None, downsample=[1, 1, 1, 1], full_time_series=True,
                               upsample=True, positive_class=[0], negative_class=[2]):

        k = 1

        self.import_data(patient_numbers=patient_number, downsample=downsample)
        epochs, epochs_type, _, session = self.get_data(full_time_series=full_time_series, upsample=upsample,
                                                      positive_class=positive_class, negative_class=negative_class)
        return epochs, epochs_type, session


# dataSegmenter = DataSegmenter()
# dataSegmenter.import_data(patient_numbers=[1,2])
##X = np.array(dataSegmenter.get_data(full_time_series=False)[0])
# print(X.shape)

## Imports
import sklearn.model_selection
import tensorflow as tf
from tensorflow import keras as keras
# import keras
# from keras import layers
# from keras import backend as K
import numpy as np
from keras.datasets import mnist
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import random
import scipy.signal as signal
from keras.backend import expand_dims as expand_dims
from dataSegmener import DataSegmenter

## Tensorflow stuff
physical_devices = tf.config.list_physical_devices('GPU')
print(physical_devices)
tf.config.set_visible_devices(devices=physical_devices[0], device_type='GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], enable=True)
# tf.config.run_functions_eagerly(True)

## Set random seed, still some randomness due to using GPU
rs = 42
random.seed(rs)
np.random.seed(rs)

## Set parameters
sampling_rate = 256
full_time_series = False
is_testing = False
n_epochs = 1
patient_start = 1
n_patients = 1
lr = 5e-4


original_dim = 52
intermediate_dim = 8
latent_dim = 13
batch_size = 32


## Reparameterization trick, from https://keras.io/examples/generative/vae/
class Sampling(keras.layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim)) ## Normal distribution mean = 0.0, sigma = 1
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


## Partly from https://www.tensorflow.org/tutorials/generative/cvae
## Overall structure from https://keras.io/examples/generative/vae/
## Rewritten as model classes, changed to one-dimensional, fixing various issues


class Encoder(keras.Model):
    def __init__(self, latent_dim):
        super(Encoder, self).__init__()
        self.latent_dim = latent_dim
        #self.inputs_ = keras.layers.InputLayer(input_shape=(original_dim, 1))
        self.inputs_ = keras.Input(shape=(original_dim, 1))
        self._set_input_layer(self.inputs_)
        self.conv1d_1 = keras.layers.Conv1D(filters=32, kernel_size=2, activation="relu", strides=1, padding="same")
        self.bn_1 = keras.layers.BatchNormalization()
        self.bn_1.trainable = False
        self.conv1d_2 = keras.layers.Conv1D(64, 3, activation="relu", strides=2, padding="same")
        self.bn_2 = keras.layers.BatchNormalization()
        self.bn_2.trainable = False
        self.conv1d_3 = keras.layers.Conv1D(64, 3, activation="relu", strides=2, padding="same")
        self.bn_3 = keras.layers.BatchNormalization()
        self.bn_3.trainable = False
        self.conv1d_4 = keras.layers.Conv1D(64, 2, activation="relu", strides=1, padding="same")
        self.bn_4 = keras.layers.BatchNormalization()
        self.bn_4.trainable = False
        self.flatten = keras.layers.Flatten()
        self.dense = keras.layers.Dense(16, activation="relu")
        self.z_mean = keras.layers.Dense(latent_dim, name="z_mean")
        self.z_log_var = keras.layers.Dense(latent_dim, name="z_log_var")
        self.z = Sampling()
        self.build()

    ## From AlwayProblem https://stackoverflow.com/questions/55235212/model-summary-cant-print-output-shape-while-using-subclass-model
    ## To get proper outputs from summary()
    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(Encoder, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def call(self, x):
        x = self.conv1d_1(x)
        x = self.bn_1(x)
        x = self.conv1d_2(x)
        x = self.bn_2(x)
        x = self.conv1d_3(x)
        x = self.bn_3(x)
        x = self.conv1d_4(x)
        x = self.bn_4(x)
        x = self.flatten(x)
        x = self.dense(x)
        z_mean = self.z_mean(x)
        z_log_var = self.z_log_var(x)
        z = self.z([z_mean, z_log_var])
        return [z_mean, z_log_var, z]


class Decoder(keras.Model):
    def __init__(self, latent_dim):
        super(Decoder, self).__init__()
        self.latent_dim = latent_dim
        self.inputs_ = keras.Input(shape=(latent_dim,))
        self._set_input_layer(self.inputs_)
        self.dense_1 = keras.layers.Dense(latent_dim, activation="relu")
        self.reshape_1 = keras.layers.Reshape((latent_dim, 1))
        self.conv1d_1 = keras.layers.Conv1DTranspose(64, 2, activation="relu", strides=1, padding="same")
        self.bn_1 = keras.layers.BatchNormalization()
        self.bn_1.trainable = False
        self.conv1d_2 = keras.layers.Conv1DTranspose(64, 3, activation="relu", strides=2, padding="same")
        self.bn_2 = keras.layers.BatchNormalization()
        self.bn_2.trainable = False
        self.conv1d_3 = keras.layers.Conv1DTranspose(64, 3, activation="relu", strides=2, padding="same")
        self.bn_3 = keras.layers.BatchNormalization()
        self.bn_3.trainable = False
        self.conv1d_4 = keras.layers.Conv1DTranspose(32, 2, activation="relu", strides=1, padding="same")
        self.bn_4 = keras.layers.BatchNormalization()
        self.bn_4.trainable = False
        self.flatten = keras.layers.Flatten()
        self.dense_2 = keras.layers.Dense(original_dim)
        self.reshape_2 = keras.layers.Reshape(target_shape=(original_dim, 1))
        self.decoder_outputs = keras.layers.Conv1DTranspose(1, 3, activation="sigmoid", padding="same")
        self.build()

    ## From AlwayProblem https://stackoverflow.com/questions/55235212/model-summary-cant-print-output-shape-while-using-subclass-model
    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(Decoder, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def call(self, x):
        x = self.dense_1(x)
        x = self.reshape_1(x)
        x = self.conv1d_1(x)
        x = self.bn_1(x)
        x = self.conv1d_2(x)
        x = self.bn_2(x)
        x = self.conv1d_3(x)
        x = self.bn_3(x)
        x = self.conv1d_4(x)
        x = self.bn_4(x)
        x = self.flatten(x)
        x = self.dense_2(x)
        x = self.reshape_2(x)
        x = self.decoder_outputs(x)
        return x


class ClassifierNN(keras.Model):
    def __init__(self, latent_dim):
        super(ClassifierNN, self).__init__()
        self.latent_dim = latent_dim
        self.inputs_ = keras.Input(shape=(latent_dim,))
        self._set_input_layer(self.inputs_)
        self.dense_1 = keras.layers.Dense(latent_dim, activation="relu")
        self.dense_2 = keras.layers.Dense(6, activation="relu")
        self.dense_3 = keras.layers.Dense(3, activation="relu")
        self.dense_4 = keras.layers.Dense(1, activation="sigmoid")
        self.build()

    ## From AlwayProblem https://stackoverflow.com/questions/55235212/model-summary-cant-print-output-shape-while-using-subclass-model
    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(ClassifierNN, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def call(self, x):
        x = self.dense_1(x)
        x = self.dense_2(x)
        x = self.dense_3(x)
        x = self.dense_4(x)
        return x


class CVAE(keras.Model):
    def __init__(self, encoder, decoder, classifier, **kwargs):
        super(CVAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.classifier = classifier
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.percent_loss_tracker = keras.metrics.Mean(name="percent_loss")
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")
        self.classifier_bce_tracker = keras.metrics.Mean(name="classifier_bce")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
            self.percent_loss_tracker,
            self.classifier_bce_tracker,
        ]

    @tf.function
    def train_step(self, data):
        data, labels = data
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            prediction = self.classifier(z)
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.binary_crossentropy(data, reconstruction), axis=1
                )
            )
            #reconstruction_loss = keras.losses.MeanSquaredError(data, reconstruction)
            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            #print(data.shape)
            #print(reconstruction.shape)
            percent_loss = tf.math.multiply(tf.abs(data-reconstruction)/tf.add(data, 1e-3), 1e-1)
            #tf.print(percent_loss)
            percent_loss = tf.reduce_mean(tf.reduce_sum(percent_loss, axis=1))
            #tf.print(percent_loss)
            classifier_bce = keras.losses.binary_crossentropy(labels, prediction, from_logits=False)
            total_loss = 1000*classifier_bce + (reconstruction_loss + kl_loss) #+ percent_loss

        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        self.percent_loss_tracker.update_state(percent_loss)
        self.classifier_bce_tracker.update_state(classifier_bce)
        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
            "percent_loss": self.percent_loss_tracker.result(),
            "classifier_bce": self.classifier_bce_tracker.result(),
        }

    def predict(self, x, batch_size):
        _, _, encoded_x = self.encoder.predict(x, batch_size=batch_size)
        decoded_x = self.decoder.predict(encoded_x, batch_size=batch_size)
        return decoded_x

    def classify(self, x, batch_size):
        _, _, encoded_x = self.encoder.predict(x, batch_size=batch_size)
        prediction = self.classifier.predict(encoded_x, batch_size=batch_size)
        return prediction

    def call(self, x):
        _, _, x = self.encoder(x)
        x = self.classifier(x)
        return x



if __name__ == "__main__":
    patients = [1, 3, 5, 9, 10, 14, 20, 21, 22, 23] # 18 and 19 don't have all channels needed to compute fp1-fp2
    #patients = [10, 14, 20, 21, 22, 23] ## 14 = 280/5xxk, 10 = 12xk/2xxk, nu 14 = 150k/302k, 10 =

    #patients = [14, 10, 1, 3, 5, 9, 20, 21, 22, 23]

    for testpatient in patients:
        ### Define model
        encoder = Encoder(latent_dim)
        decoder = Decoder(latent_dim)
        classifier = ClassifierNN(latent_dim)
        cvae = CVAE(encoder, decoder, classifier)
        cvae.compile(optimizer=keras.optimizers.Adam(lr=lr))
        cvae.encoder.summary()
        cvae.decoder.summary()
        cvae.classifier.summary()
        cae = cvae

        ### Import data
        ## Class types:
        ## 0: Seizure
        ## 1: Pre-ictal (30 minutes before seizure-seizure start)
        ## 2: Inter-ictal (start to 4 hours before seizure, 4 hours after seizure to end)
        ## 3: Everything else

        dataSegmenter = DataSegmenter(path_to_data="../data/chbmit")

        trainpatients = [i for i in patients if i != testpatient]
        # dataSegmenter.import_data(trainpatients, downsample=[1, 1, 1, 1])
        # print(len(dataSegmenter.epochs))
        # testpatient = 1
        # dataSegmenter.import_data([1], downsample=[1, .1, .1, .1])

        epochs, epochs_type = dataSegmenter.smarter_loader('saveddata/', patient_numbers=trainpatients, full_time_series=False, upsample=True, positive_class=[0],
                                                       negative_class=[1, 2, 3])

        epochs_type = np.expand_dims(epochs_type, axis=-1)
        print(epochs.shape)
        train_ds = tf.data.Dataset.from_tensor_slices((epochs, epochs_type)).shuffle(100000).batch(32)

        # epochs, epochs_type = dataSegmenter.smarter_loader('saveddata/', patient_numbers=[testpatient], full_time_series=False, upsample=True, positive_class=[0],
        #                                                negative_class=[1, 2, 3])
        # epochs_type = np.expand_dims(epochs_type, axis=-1)
        # val_ds = tf.data.Dataset.from_tensor_slices((epochs, epochs_type))

        if is_testing:
            dataSegmenter = DataSegmenter(path_to_data="../data/chbmit")
            dataSegmenter.import_data([23], downsample=[1, .1, .1, .1])
            x_test, y_test, _, _ = dataSegmenter.get_data(full_time_series=False, upsample=True, positive_class=[0],
                                                               negative_class=[1, 2, 3])
            x_test = np.squeeze(x_test)
        # test_ds = tf.data.Dataset.from_tensor_slices((epochs, epochs_type)).batch(32)

        # x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(epochs, labels, test_size=0.2, stratify=labels)
        # pds = tf.data.Dataset.from_tensor_slices((expand_dims(x_train[y_train == 1], axis=-1), expand_dims(y_train[y_train == 1], axis=-1)))
        # pds = pds.shuffle(100000).repeat()
        # nds = tf.data.Dataset.from_tensor_slices((expand_dims(x_train[y_train == 0], axis=-1), expand_dims(y_train[y_train == 0], axis=-1)))
        # nds = nds.shuffle(100000).repeat()
        # resampled_ds = tf.data.Dataset.sample_from_datasets([pds, nds], weights=[0.5, 0.5])
        # resampled_ds = resampled_ds.batch(32).prefetch(2)
        # spe = np.ceil(2.0*no_seizure.shape[0]/32)
        # train_ds = resampled_ds


        cae.fit(train_ds,
                #steps_per_epoch=spe,
                epochs=n_epochs,
                # validation_data=val_ds,
                batch_size=batch_size)

        ## Save weights
        cae.save_weights("./weightsCVAE/seiznonseiz_e{0}_testp{1}_fp1fp2/final".format(n_epochs, testpatient))

        ## Testing
        if is_testing:
            predictions = cae.classify(np.expand_dims(x_test, axis=-1), batch_size=32)
            predictions = np.squeeze(predictions.flatten())
            pred_labels = np.rint(predictions)

            correct = np.sum(pred_labels == y_test)
            acc = correct / y_test.shape[0]
            print(acc)

            tp = pred_labels[pred_labels == y_test]
            tp = np.sum(tp == 1)
            fp = pred_labels[pred_labels != y_test]
            fp = np.sum(fp == 1)
            tn = pred_labels[pred_labels == y_test]
            tn = np.sum(tn == 0)
            fn = pred_labels[pred_labels != y_test]
            fn = np.sum(fn == 0)
            tpr = tp / (tp + fn)
            tnr = tn / (tn + fp)
            fpr = fp / (fp + tn)
            fnr = fn / (fn + tp)

            print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
            print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)


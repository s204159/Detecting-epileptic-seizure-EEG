from CVAEModel import CVAE, Encoder, Decoder, ClassifierNN
import CVAEModel_finetuning as CVAE_
from NNEpilepticModel import CNNModel
from dataSegmener import DataSegmenter
from tensorflow import keras as keras
from statsmodels.stats.contingency_tables import mcnemar
import tensorflow as tf
import sklearn

import numpy as np
import random
import tensorflow as tf

## Set random seed, still some randomness due to using GPU
rs = 42
random.seed(rs)
np.random.seed(rs)

## Set parameters
sampling_rate = 256
full_time_series = False
is_testing = False
n_epochs = 1
patient_start = 1
n_patients = 1
lr = 5e-4

original_dim = 52
intermediate_dim = 8
latent_dim = 13
batch_size = 32

patients = [1, 3, 5, 9, 10, 14, 20, 23] # 21 and 22 cannot be done with model


def generate_table(A, B):
    table = [[np.sum((A == 1) & (B == 1)), np.sum((A == 1) & (B == 0))],
             [np.sum((A == 0) & (B == 1)), np.sum((A == 0) & (B == 0))]]
    return table

success_arr_CVAEft, total_arr_CVAEft = [],[]
success_arr_CVAE, total_arr_CVAE = [],[]

def get_number_of_successful_classifications(true_labels, predictions_labels, cut_off):
    boo = true_labels.astype(bool)
    indices = np.nonzero(boo[1:] != boo[:-1])[0] + 1

    splitted_true_labels = np.split(true_labels, indices)
    splitted_true_labels = splitted_true_labels[0::2] if boo[0] else splitted_true_labels[1::2]

    splitted_predictions_labels = np.split(predictions_labels, indices)
    splitted_predictions_labels = splitted_predictions_labels[0::2] if boo[0] else splitted_predictions_labels[1::2]

    success = 0
    total = 0

    for arr in splitted_predictions_labels:
        if np.any(arr >= cut_off):
            success += 1

        total += 1

    return success, total


total_mcnemar = np.array([[0, 0],
                          [0, 0]])
total_CVAEft_tp = 0
total_CVAEft_fp = 0
total_CVAEft_tn = 0
total_CVAEft_fn = 0
total_CVAE_tp = 0
total_CVAE_fp = 0
total_CVAE_tn = 0
total_CVAE_fn = 0

for testpatient in patients:

    # Build models
    encoder = Encoder(latent_dim)
    decoder = Decoder(latent_dim)
    classifier = ClassifierNN(latent_dim)
    cvae = CVAE(encoder, decoder, classifier)
    cvae.compile(optimizer=keras.optimizers.Adam(lr=lr))
    cvae(np.zeros((1, 52, 1)))
    cvae.build(input_shape=(1, 52, 1))
    cvae.load_weights("./weightsCVAE/ft_seiznonseiz_e1_testp{0}_fp1fp2_lr1e-5_ups1000_e5_nopretrain/final".format(testpatient)).expect_partial()

    ftencoder = CVAE_.Encoder(latent_dim)
    ftdecoder = CVAE_.Decoder(latent_dim)
    ftclassifier = CVAE_.ClassifierNN(latent_dim)
    cvaeft = CVAE_.CVAE(ftencoder, ftdecoder, ftclassifier)
    cvaeft.classifier.trainable = False
    cvaeft.compile(optimizer=keras.optimizers.Adam(lr=1e-5))
    cvaeft(np.zeros((1, 52, 1)))
    cvaeft.build(input_shape=(1, 52, 1))
    cvaeft.load_weights("./weightsCVAE/ft_seiznonseiz_e1_testp{0}_fp1fp2_lr1e-5_ups1000_e5/final".format(testpatient)).expect_partial()


    # Import data
    epochs = np.load("ordered_data/epochs_patient_{0}.npy".format(testpatient))
    epochs_type = np.load("ordered_data/type_patient_{0}.npy".format(testpatient))
    # epochs = np.load("saveddatatest/{0}_sorted_epochs.npy".format(testpatient))
    # epochs_type = np.load("saveddatatest/{0}_sorted_epochs_type.npy".format(testpatient))
    #epochs = np.expand_dims(epochs, axis=-1)
    # epochs = np.moveaxis(epochs, 1, 2)
    epochs = np.expand_dims(np.squeeze(epochs), axis=-1)
    # epochs_type = np.array([1 if x == 0 else 0 for x in epochs_type])
    # epochs_type = np.expand_dims(epochs_type, axis=-1)

    ## Split on sorted data
    _, x_test, _, y_test = sklearn.model_selection.train_test_split(epochs, epochs_type, test_size=0.5, shuffle=False)
    # print(y_testft.shape)
    x_test = np.squeeze(x_test)
    # print(y_test.shape)
    print("Patient " + str(testpatient))
    cvaeFTcvae = True

    if cvaeFTcvae:
        # Test on CVAEft
        CVAEft_predictions = cvaeft.classify(np.expand_dims(x_test, axis=-1), batch_size=32)
        CVAEft_predictions = np.squeeze(CVAEft_predictions.flatten())
        CVAEft_pred_labels = np.rint(CVAEft_predictions)
        CVAEft_correct = (CVAEft_pred_labels == y_test)
        n_CVAEft_correct = np.sum(CVAEft_pred_labels == y_test)
        # correct = np.sum(CVAE_pred_labels == y_test)
        # acc = correct / y_test.shape[0]
        # print(acc)
        #
        tp = CVAEft_pred_labels[CVAEft_pred_labels == y_test]
        total_CVAEft_tp += np.sum(tp == 1)
        fp = CVAEft_pred_labels[CVAEft_pred_labels != y_test]
        total_CVAEft_fp += np.sum(fp == 1)
        tn = CVAEft_pred_labels[CVAEft_pred_labels == y_test]
        total_CVAEft_tn += np.sum(tn == 0)
        fn = CVAEft_pred_labels[CVAEft_pred_labels != y_test]
        total_CVAEft_fn += np.sum(fn == 0)

        success, total = get_number_of_successful_classifications(y_test, CVAEft_predictions, .5)
        success_arr_CVAEft.append(success)
        total_arr_CVAEft.append(total)

        # tpr = tp / (tp + fn)
        # tnr = tn / (tn + fp)
        # fpr = fp / (fp + tn)
        # fnr = fn / (fn + tp)
        #
        # print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
        # print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)

        # Test on CVAE
        CVAE_predictions = cvae.classify(np.expand_dims(x_test, axis=-1), batch_size=32)
        CVAE_predictions = np.squeeze(CVAE_predictions.flatten())
        CVAE_pred_labels = np.rint(CVAE_predictions)
        CVAE_correct = (CVAE_pred_labels == y_test)
        n_CVAE_correct = np.sum(CVAE_pred_labels == y_test)
        # correct = np.sum(CVAE_pred_labels == y_test)
        # acc = correct / y_test.shape[0]
        # print(acc)
        #
        tp = CVAE_pred_labels[CVAE_pred_labels == y_test]
        total_CVAE_tp += np.sum(tp == 1)
        fp = CVAE_pred_labels[CVAE_pred_labels != y_test]
        total_CVAE_fp += np.sum(fp == 1)
        tn = CVAE_pred_labels[CVAE_pred_labels == y_test]
        total_CVAE_tn += np.sum(tn == 0)
        fn = CVAE_pred_labels[CVAE_pred_labels != y_test]
        total_CVAE_fn += np.sum(fn == 0)

        success, total = get_number_of_successful_classifications(y_test, CVAE_predictions, .5)
        success_arr_CVAE.append(success)
        total_arr_CVAE.append(total)
        # Compare results

        table = generate_table(CVAEft_correct,CVAE_correct)
        print(table)
        results = mcnemar(table, exact=False, correction=False)
        print(results)
        total_mcnemar += np.array(table)
        # print(total_mcnemar)

print("Total McNemar")
print(total_mcnemar)
print(mcnemar(total_mcnemar, exact=False, correction=False))
print("Total accuracy")
print("CVAEft: " + str((total_CVAEft_tp+total_CVAEft_tn)/(total_CVAEft_fn+total_CVAEft_fp+total_CVAEft_tn+total_CVAEft_tp)))
print("CVAE: " + str((total_CVAE_tp+total_CVAE_tn)/(total_CVAE_fn+total_CVAE_fp+total_CVAE_tn+total_CVAE_tp)))
print("Confusion")
print("CVAEft:")
print([[total_CVAEft_tp, total_CVAEft_fp],
       [total_CVAEft_fn, total_CVAEft_tn]])
print("CVAE: ")
print([[total_CVAE_tp, total_CVAE_fp],
       [total_CVAE_fn, total_CVAE_tn]])
print("Balanced accuracy")
pos = total_CVAEft_tp+total_CVAEft_fn
neg = total_CVAEft_tn+total_CVAEft_fp
pfn = total_CVAEft_fn/pos
ptp = total_CVAEft_tp/pos
n = neg-pos
total_CVAEft_fn += pfn*n
total_CVAEft_tp += ptp*n
print("CVAEft: " + str((total_CVAEft_tp+total_CVAEft_tn)/(total_CVAEft_fn+total_CVAEft_fp+total_CVAEft_tn+total_CVAEft_tp)))
pos = total_CVAE_tp+total_CVAE_fn
neg = total_CVAE_tn+total_CVAE_fp
pfn = total_CVAE_fn/pos
ptp = total_CVAE_tp/pos
n = neg-pos
total_CVAE_fn += pfn*n
total_CVAE_tp += ptp*n
print("CVAE: " + str((total_CVAE_tp+total_CVAE_tn)/(total_CVAE_fn+total_CVAE_fp+total_CVAE_tn+total_CVAE_tp)))
print("Balanced confusion")
print("CVAEft:")
print([[total_CVAEft_tp, total_CVAEft_fp],
       [total_CVAEft_fn, total_CVAEft_tn]])
print("CVAE: ")
print([[total_CVAE_tp, total_CVAE_fp],
       [total_CVAE_fn, total_CVAE_tn]])

print("success and totals total")
print("CVAEft:")
print(success_arr_CVAEft,total_arr_CVAEft)
print("CVAE: ")
print(success_arr_CVAE,total_arr_CVAE)

    #
    # # Test on finetuned CVAE
    # epochs = np.load("saveddatatest/{0}_sorted_epochs.npy".format(testpatient))
    # epochs_type = np.load("saveddatatest/{0}_sorted_epochs_type.npy".format(testpatient))
    # #epochs = np.expand_dims(epochs, axis=-1)
    # epochs = np.moveaxis(epochs, 1, 2)
    # epochs = np.expand_dims(np.squeeze(epochs), axis=-1)
    # epochs_type = np.array([1 if x == 0 else 0 for x in epochs_type])
    # # epochs_type = np.expand_dims(epochs_type, axis=-1)
    #
    # ## Split on sorted data
    # _, x_testft, _, y_testft = sklearn.model_selection.train_test_split(epochs, epochs_type, test_size=0.5)
    # # print(y_testft.shape)
    # encoder = CVAE_.Encoder(latent_dim)
    # decoder = CVAE_.Decoder(latent_dim)
    # classifier = CVAE_.ClassifierNN(latent_dim)
    # cvaeft = CVAE_.CVAE(encoder, decoder, classifier)
    # cvaeft.classifier.trainable = False
    # cvaeft.compile(optimizer=keras.optimizers.Adam(lr=1e-5))
    # cvaeft(np.zeros((1, 52, 1)))
    # cvaeft.build(input_shape=(1, 52, 1))
    # cvaeft.load_weights("./weightsCVAE/ft_seiznonseiz_e1_testp{0}_fp1fp2_lr1e-5_ups1000_e5/final".format(testpatient)).expect_partial()
    #
    # CVAEft_predictions = cvaeft.classify(x_testft, batch_size=32)
    # CVAEft_predictions = np.squeeze(CVAEft_predictions.flatten())
    # CVAEft_pred_labels = np.rint(CVAEft_predictions)
    # CVAEft_correct = (CVAEft_pred_labels == y_testft)
    # correct = np.sum(CVAEft_pred_labels == y_testft)
    # acc = correct / y_testft.shape[0]
    # print(acc)
    #
    # tp = CVAEft_pred_labels[CVAEft_pred_labels == y_testft]
    # tp = np.sum(tp == 1)
    # fp = CVAEft_pred_labels[CVAEft_pred_labels != y_testft]
    # fp = np.sum(fp == 1)
    # tn = CVAEft_pred_labels[CVAEft_pred_labels == y_testft]
    # tn = np.sum(tn == 0)
    # fn = CVAEft_pred_labels[CVAEft_pred_labels != y_testft]
    # fn = np.sum(fn == 0)
    # tpr = tp / (tp + fn)
    # tnr = tn / (tn + fp)
    # fpr = fp / (fp + tn)
    # fnr = fn / (fn + tp)
    #
    # print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
    # print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)
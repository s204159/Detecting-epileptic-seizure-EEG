import CVAEModel_finetuning as CVAE_
#from CVAEModel import CVAE, Encoder, Decoder, ClassifierNN
from NNEpilepticModel import CNNModel
from dataSegmener import DataSegmenter
from tensorflow import keras as keras
from statsmodels.stats.contingency_tables import mcnemar
import tensorflow as tf

import sklearn
import numpy as np
import random
import tensorflow as tf

import os

## Set random seed, still some randomness due to using GPU
rs = 42
random.seed(rs)
np.random.seed(rs)

## Set parameters
sampling_rate = 256
full_time_series = False
is_testing = False
saving = False
n_epochs = 1
patient_start = 1
n_patients = 1
lr = 1e-5

original_dim = 52
intermediate_dim = 8
latent_dim = 13
batch_size = 32

patients = [1, 3, 5, 9, 10, 14, 20, 23] # 18 and 19 don't have all channels needed to compute fp1-fp2
# patient 21 and 22 cannot be finetuned with current model

if saving:
    for testpatient in patients:

        # Build models

        dataSegmenter = DataSegmenter(path_to_data="../data/chbmit")

        epochs, epochs_type, session = dataSegmenter.smarter_loader_session(patient_number=[testpatient],
                                                      full_time_series=False, upsample=False, positive_class=[0],
                                                      negative_class=[1, 2, 3])

        epochs_type = np.squeeze(epochs_type)

        ## Sort data by session number so we don't use the future to predict the past
        idx = np.argsort(session)
        session = session[idx]
        epochs = epochs[idx]
        epochs_type = epochs_type[idx]

        np.save("saveddatatest/{0}_sorted_epochs.npy".format(testpatient), epochs)
        np.save("saveddatatest/{0}_sorted_epochs_type.npy".format(testpatient), epochs_type)
        np.save("saveddatatest/{0}_sorted_session.npy".format(testpatient), session)

for testpatient in patients:
    print("Patient " + str(testpatient))
    epochs = np.load("ordered_data/epochs_patient_{0}.npy".format(testpatient))
    epochs_type = np.load("ordered_data/type_patient_{0}.npy".format(testpatient))
    # epochs = np.load("saveddatatest/{0}_sorted_epochs.npy".format(testpatient))
    # epochs_type = np.load("saveddatatest/{0}_sorted_epochs_type.npy".format(testpatient))
    #epochs = np.expand_dims(epochs, axis=-1)
    # epochs = np.moveaxis(epochs, 1, 2)
    # epochs_type = np.array([1 if x == 0 else 0 for x in epochs_type])

    ## Split on sorted data
    x_train, _, y_train, _ = sklearn.model_selection.train_test_split(epochs, epochs_type, test_size=0.5, shuffle=False)
    upsample = True
    if upsample:
        positive_class = [1]
        negative_class = [0]
        pos_features = x_train[[epoch_type in positive_class for epoch_type in y_train]]
        neg_features = x_train[[epoch_type in negative_class for epoch_type in y_train]]
        pos_labels = np.full(len(pos_features), 1, dtype=int)  # epochs_type[(epochs_type == 0)]
        neg_labels = np.full(len(neg_features), 0, dtype=int)  # epochs_type[(epochs_type == 2)]
        ids = np.arange(len(pos_features))
        choices = np.random.choice(ids, len(neg_features))
        res_pos_features = pos_features[choices]
        res_pos_labels = pos_labels[choices]
        resampled_features = np.concatenate([res_pos_features, neg_features], axis=0)
        resampled_labels = np.concatenate([res_pos_labels, neg_labels], axis=0)
        order = np.arange(len(resampled_labels))
        np.random.shuffle(order)
        resampled_features = resampled_features[order]
        resampled_labels = resampled_labels[order]
        print(resampled_features.shape)
        resampled_features = np.expand_dims(np.squeeze(resampled_features), axis=-1)
        resampled_labels = np.expand_dims(resampled_labels, axis=-1)
        train_ds = tf.data.Dataset.from_tensor_slices((resampled_features, resampled_labels)).batch(32)
    else:
        x_train = np.expand_dims(np.squeeze(x_train), axis=-1)
        y_train = np.expand_dims(y_train, axis=-1)
        train_ds = tf.data.Dataset.from_tensor_slices((x_train, y_train)).batch(32)
    # test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)

    encoder = CVAE_.Encoder(latent_dim)
    decoder = CVAE_.Decoder(latent_dim)
    classifier = CVAE_.ClassifierNN(latent_dim)
    cvae = CVAE_.CVAE(encoder, decoder, classifier)
    cvae.classifier.trainable = False
    lr=1e-5
    lrstr = "1e-5"
    cvae.compile(optimizer=keras.optimizers.Adam(lr=lr))
    cvae(np.zeros((1, 52, 1)))
    cvae.build(input_shape=(1, 52, 1))
    cvae.load_weights("./weightsCVAE/seiznonseiz_e1_testp{0}_fp1fp2/final".format(testpatient)).expect_partial()

    cvae.fit(train_ds,
            # steps_per_epoch=spe,
            epochs=5,
            batch_size=batch_size)

    cvae.save_weights("./weightsCVAE/ft_seiznonseiz_e{0}_testp{1}_fp1fp2_lr{2}_ups1000_e5/final".format(n_epochs, testpatient, lrstr))








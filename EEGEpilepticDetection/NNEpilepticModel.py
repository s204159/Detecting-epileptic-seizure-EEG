from dataSegmener import DataSegmenter
from keras.layers import Dense, Flatten, Conv2D, Conv1D, MaxPool1D
from keras import Model

import numpy as np
import tensorflow as tf

print("TensorFlow version:", tf.__version__)

physical_devices = tf.config.list_physical_devices('GPU')
print(physical_devices)


class CNNModel(Model):
    def __init__(self):
        super(CNNModel, self).__init__()

        self.inputs_ = tf.keras.Input(shape=(52, 1), name="hello")
        self._set_input_layer(self.inputs_)

        self.conv1 = Conv1D(filters=32, kernel_size=4, strides=2, activation='relu', padding="same",
                            input_shape=(52, 1))
        self.conv2 = Conv1D(filters=32, kernel_size=3, strides=1, activation='relu', padding="same")
        self.conv3 = Conv1D(filters=32, kernel_size=2, strides=2, activation='relu', padding="same")
        self.flatten = Flatten()
        self.d1 = Dense(128, activation='relu')
        self.d2 = Dense(128, activation='relu')
        self.d3 = Dense(128, activation='relu')
        self.d4 = Dense(64, activation='relu')
        self.d5 = Dense(1, activation='sigmoid')

    def call(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.flatten(x)
        x = self.d1(x)
        x = self.d2(x)
        x = self.d3(x)
        x = self.d4(x)
        x = self.d5(x)
        return x

    def _set_input_layer(self, inputs):
        """add inputLayer to model and display InputLayers in model.summary()

        Args:
            inputs ([dict]): the result from `tf.keras.Input`
        """
        if isinstance(inputs, dict):
            self.inputs_layer = {n: tf.keras.layers.InputLayer(input_tensor=i, name=n)
                                 for n, i in inputs.items()}
        elif isinstance(inputs, (list, tuple)):
            self.inputs_layer = [tf.keras.layers.InputLayer(input_tensor=i, name=i.name)
                                 for i in inputs]
        elif tf.is_tensor(inputs):
            self.inputs_layer = tf.keras.layers.InputLayer(input_tensor=inputs, name=inputs.name)

    def build(self):
        super(CNNModel, self).build(self.inputs_.shape if tf.is_tensor(self.inputs_) else self.inputs_)
        _ = self.call(self.inputs_)

    def classify(self, x, batch_size):
        prediction = self.predict(x, batch_size=batch_size)
        return prediction


if __name__ == "__main__":

    is_testing = False
    save_weights = True
    n_epochs = 1
    batch_size = 32

    patients = [1, 3, 5, 9, 10, 14, 20, 21, 22, 23]  # 18 and 19 don't have all channels needed to compute fp1-fp2
    # patients = [10, 14, 20, 21, 22, 23] ## 14 = 280/5xxk, 10 = 12xk/2xxk, nu 14 = 150k/302k, 10 =

    # patients = [14, 10, 1, 3, 5, 9, 20, 21, 22, 23]

    for testpatient in patients:
        ### Define model
        cnn = CNNModel()
        cnn.build()
        cnn.compile(optimizer=tf.keras.optimizers.Adam(),
                    loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                    metrics=['accuracy'])
        cnn.summary()

        ### Import data
        ## Class types:
        ## 0: Seizure
        ## 1: Pre-ictal (30 minutes before seizure-seizure start)
        ## 2: Inter-ictal (start to 4 hours before seizure, 4 hours after seizure to end)
        ## 3: Everything else

        dataSegmenter = DataSegmenter(path_to_data="../../chbmit")
        trainpatients = [i for i in patients if i != testpatient]

        epochs, epochs_type = dataSegmenter.smarter_loader('saveddata/', patient_numbers=trainpatients,
                                                           full_time_series=False, upsample=True, positive_class=[0],
                                                           negative_class=[1, 2, 3])

        epochs_type = np.expand_dims(epochs_type, axis=-1)
        print(epochs.shape)
        train_ds = tf.data.Dataset.from_tensor_slices((epochs[::], epochs_type[::])).shuffle(100000).batch(32)

        # resampled_steps_per_epoch = np.ceil(2.0 * neg / batch_size)

        ## Testing
        if is_testing:
            dataSegmenter = DataSegmenter(path_to_data="../../chbmit")
            dataSegmenter.import_data([testpatient], downsample=[1, .1, .1, .1])
            x_test, y_test, _, _ = dataSegmenter.get_data(full_time_series=False, upsample=True, positive_class=[0],
                                                          negative_class=[1, 2, 3])
            x_test = np.squeeze(x_test)

            cnn.fit(train_ds,
                    # steps_per_epoch=spe,
                    epochs=n_epochs,
                    batch_size=batch_size,
                    steps_per_epoch=3800,
                    validation_data=(np.expand_dims(x_test, axis=-1), y_test))

            predictions = cnn.classify(np.expand_dims(x_test, axis=-1), batch_size=32)
            predictions = np.squeeze(predictions.flatten())
            pred_labels = np.rint(predictions)

            correct = np.sum(pred_labels == y_test)
            acc = correct / y_test.shape[0]
            print(acc)

            tp = pred_labels[pred_labels == y_test]
            tp = np.sum(tp == 1)
            fp = pred_labels[pred_labels != y_test]
            fp = np.sum(fp == 1)
            tn = pred_labels[pred_labels == y_test]
            tn = np.sum(tn == 0)
            fn = pred_labels[pred_labels != y_test]
            fn = np.sum(fn == 0)
            tpr = tp / (tp + fn)
            tnr = tn / (tn + fp)
            fpr = fp / (fp + tn)
            fnr = fn / (fn + tp)

            print("TP: ", tp, "\nFP: ", fp, "\nTN: ", tn, "\nFN: ", fn)
            print("TPR: ", tpr, "\nFPR: ", fpr, "\nTNR: ", tnr, "\nFNR: ", fnr)
        else:
            cnn.fit(train_ds,
                    # steps_per_epoch=spe,
                    epochs=n_epochs,
                    batch_size=batch_size,
                    # steps_per_epoch=3800,
                    )

        if save_weights:
            cnn.save_weights("./weights/seiznonseiz_e{0}_testp{1}_fp1fp2/CNN_final".format(n_epochs, testpatient))
    # model.build()
    # print(model.summary())
    #
    # model.compile(optimizer=tf.keras.optimizers.Adam(),
    #               loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
    #               metrics=['accuracy'])
    #
    #
    # model.fit(train_ds, epochs=6)
    # model.evaluate(test_ds, verbose=2)

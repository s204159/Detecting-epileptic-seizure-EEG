## Imports
import sklearn.model_selection
import tensorflow as tf
from tensorflow import keras as keras
# import keras
# from keras import layers
# from keras import backend as K
import numpy as np
from keras.datasets import mnist
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import random
import scipy.signal as signal
from keras.backend import expand_dims as expand_dims
from dataSegmener import DataSegmenter

## Set random seed, still some randomness due to using GPU
rs = 42
random.seed(rs)
np.random.seed(rs)

## Set parameters
sampling_rate = 256
full_time_series = False
is_testing = False
n_epochs = 5
patient_start = 1
n_patients = 1
lr = 5e-4


original_dim = 52
intermediate_dim = 8
latent_dim = 13
batch_size = 32

patients = [1, 3, 5, 9, 10, 14, 20, 21, 22, 23] # 18 and 19 don't have all channels needed to compute fp1-fp2

for testpatient in patients:
    ### Import data
    ## Class types:
    ## 0: Seizure
    ## 1: Pre-ictal (30 minutes before seizure-seizure start)
    ## 2: Inter-ictal (start to 4 hours before seizure, 4 hours after seizure to end)
    ## 3: Everything else

    # dataSegmenter = DataSegmenter(path_to_data="../data/chbmit")

    trainpatients = [i for i in patients]# if i != testpatient]
    # dataSegmenter.import_data(trainpatients, downsample=[1, 1, 1, 1])
    # print(len(dataSegmenter.epochs))
    # testpatient = 1
    # dataSegmenter.import_data([1], downsample=[1, .1, .1, .1])
    for p in trainpatients:
        dataSegmenter = DataSegmenter(path_to_data="../data/chbmit")
        dataSegmenter.smart_saver('saveddata/', patient_numbers=[p], full_time_series=False, upsample=True, positive_class=[0],
                                                       negative_class=[1, 2, 3])
        print(len(dataSegmenter.epochs))

